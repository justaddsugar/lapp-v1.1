// Copyright Jaap Harmsma & Rob Hopster 2016-2017 **Just Add Sugar B.V.**
// info@justaddsugar.nl of http://www.justaddsugar.nl

angular.module('starter.services', [])




// Controleren of de gebruiker toegang heeft. Gegevens opvragen van de gebruiker en eventueel het useraccount activeren bij het LAS.  -----------------------------------------------------------------------------------------------------
// Geoptimaliseerd = JA
.factory('ActivateService', function($http, ApiEndpoint) {
  var promise;
  var objService = {
	activeEmail: function(param){
      console.log(param);
      promise = $http.post(ApiEndpoint.url+"mod.php?id=38&username=VwCtSjVkdd3WQyBrEFYHg7TsZy530xXJ&password=EXIbcxklGgL7uZfwq9VzbpqkcZlFzTQe&action=run&mid=38&email="+param.email+"&deviceid="+param.deviceid).then(function (response) {
        return response.data;
      });
      return promise;
    },
  checkIfMemberExistInMas: function(param){
        promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9961110420990736&method=GetCurrentUserDetails").then(function (response) {
        return response.data;
      });
      return promise;
    },
	getCurrentUserDetails: function(param){
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9961110420990736&method=GetCurrentUserDetails").then(function (response) {
        return response.data;
      });
      return promise;
    }
  };
  return objService;
})
// Einde




// Mijn Circle, Alle Circles en aanpassen profiel pagina functies-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Geoptimaliseerd = JA
.factory('LadiesService', function($http, ApiEndpoint) {
  var promise;
  var objService = {
  GetNewestMembersHavingAPicture: function(param){
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=GetNewestMembersHavingAPicture").then(function (response) {
        return response.data;
      });
      return promise;
    },
	SearchCirclers: function(param){
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=SearchMembers&data="+param.searchWord).then(function (response) {
        return response.data;
      });
      return promise;
    },
	SearchCircle: function(param){
      console.log(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=GetCircleData&data="+param.searchWord);
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=GetCircleData&data="+param.searchWord).then(function (response) {
        return response.data;
      });
      return promise;
    },
	myCircle: function(param){
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=GetCircleData").then(function (response) {
        return response.data;
      });
      return promise;
    },
	getMembersOfCircle: function(param){
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=GetMembersOfCircle&data="+param.tbl+"").then(function (response) {
        return response.data;
      });
      return promise;
    },
	getOldMembersOfCircle: function(param){
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=GetMembersOfCircle&IncludeOldMembers=1").then(function (response) {
        return response.data;
      });
      return promise;
    },
  getMembersOfCircle1: function(param){
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=GetMembersOfCircle&data="+param.tbl+"").then(function (response) {
        return response.data;
      });
      return promise;
    },
	getOldMembersOfCircle1: function(param){
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=GetMembersOfCircle&data="+param.tbl+"&IncludeOldMembers=1").then(function (response) {
        return response.data;
      });
      return promise;
    },
	updateMyProfile: function(param){
      console.log(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=SetCurrentUserDetails&data="+JSON.stringify(param.data));
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=SetCurrentUserDetails&data="+JSON.stringify(param.data)).then(function (response) {
        return response.data;
      });
      return promise;
    }
  };
  return objService;
})
// Einde





// Alle Circles ophalen uit het LAS ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Geoptimaliseerd = JA
.factory('CircleService', function($http, ApiEndpoint) {
  var promise;
  var objService = {
    all: function(param){
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=GetCircleData").then(function (response) {
        return response.data;
      });
      return promise;
    }
  };
  return objService;
})
// Einde





// Nieuwsfeed ophalen uit Wordpress omgeving Ladies Circle-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Geoptimaliseerd = JA
.factory('NewsService', function($http, ApiEndpoint) {
  var promise;
  var objService = {
    all: function(param){
      promise = $http.post("https://lappadmin.nl/?json=get_recent_posts").then(function (response) {
        return response.data;
      });
      return promise;
    }
  };
  return objService;
})
// Einde





// Agenda items ophalen en toevoegen aan het LAS -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Geoptimaliseerd = JA
.factory('AgendaService', function($http, ApiEndpoint) {
  var promise;
  var objService = {
    all: function(param){
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=GetActivities").then(function (response) {
        return response.data;
      });
      return promise;
    },
	myCircleAgenda: function(param){
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=GetActivities&scope=Circle").then(function (response) {
        return response.data;
      });
      return promise;
    },
  myDistrictAgenda: function(param){
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=GetActivities").then(function (response) {
        return response.data;
      });
      return promise;
    },
  // Begin code Jaap
  myIcalimport: function(param){
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=GetiCalCalendarURL").then(function (response) {
        return response.data;
      });
      return promise;
    },
  // End code Jaap
  AddAgenda: function(param){
      console.log(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=SetActivity&data="+JSON.stringify(param.data)); 
      promise = $http.get(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=SetActivity&data="+JSON.stringify(param.data)).then(function (response) {
        return response.data;
      });
      return promise;
    }
  };
  return objService;
})
// Einde





// Statische pagina's ophalen en statistieken LCNL----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Geoptimaliseerd = JA
.factory('NTRService', function($http, ApiEndpoint) {
  var promise;
  var objService = {
    
  all: function(param){
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=GetLCNLStatistics").then(function (response) {
        return response.data;
      });
      return promise;
    },
	myCircleNTR: function(param){
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=GetLCNLStatistics&scope=Circle").then(function (response) {
        return response.data;
      });
      return promise;
    },
  detailNTR: function(param){
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=GetLCNLStatistics&scope=Home").then(function (response) {
        return response.data;
      });
      return promise;
    },
	allList: function(param){
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=GetAvailableStaticPages").then(function (response) {
        return response.data;
      });
      return promise;
    },
	getStaticPage: function(param){
      promise = $http.post(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=GetStaticPage&data="+param.id).then(function (response) {
        return response.data;
      });
      return promise;
    }
  };
  return objService;
})
// Einde





// Google maps plugin voor Ladies op kaart ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Geoptimaliseerd = JA
.factory('GeoService', function($http, ApiEndpoint) {
  var promise;
  var objService = {
    Geocoding: function(address){
      promise = $http.post("http://maps.google.com/maps/api/geocode/json?address="+address+"&sensor=false").then(function (response) {
        return response.data;
      });
      return promise;
    }
  };
  return objService;
})
// Einde





// Code Jaap. Laten staan om mee te testen. ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Geoptimaliseerd = JA
.factory('SecretService', function($http, ApiEndpoint) {
  var promise;
  var objService = {

    AddSecret: function(param){
       
      alert(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=SetAboutMe&member="+param.member_id+"&data="+param.data);
      promise = $http.get(ApiEndpoint.url+"modules.php?action=run&mid=68&username="+param.email+"&password="+param.password+"&deviceid=9668702117633076&method=SetAboutMe&member="+param.member_id+"&data="+param.data).then(function (response) {
            return response.data;
      });
            return promise;
      }
  };
  return objService;
})
// Einde code Jaap





// Code voor formulieren. -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Geoptimaliseerd = JA
.factory('AJAXService', function(ApiEndpoint) {
  var promise;
  var objService = {
    ajax_get:function(url){
    return new Promise(function(resolve, reject) {
      var req = new XMLHttpRequest();
      req.open('GET', url);
      req.onload = function() {
        if (req.status == 200) {
        resolve(req.response);
        }
        else {
        reject(req.statusText);
        }
      };
      req.onerror = function(e) {
        reject(e);
      };
      req.send();
    });
  },
  
  ajax_post:function(url, param){
    return new Promise(function(resolve, reject) {
      var req = new XMLHttpRequest();
      req.open('POST', ApiEndpoint.url+url);
      req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      req.onload = function() {
        if (req.status == 200) {
        resolve(req.response);
        }
        else {
        reject(req.statusText);
        }
      };
      req.onerror = function() {
        reject("Network Error");
      };
      req.send(param);
    });
  }
  };
  return objService;
})
// Einde





// User & Wachtwoord opslaan op lokaal device. -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Geoptimaliseerd = JA
.factory('Storage', function(){
  return {
    getItem: function (key) {
      return localStorage.getItem(key);
    },
    getObject: function (key) {
      return angular.fromJson(localStorage.getItem(key));
    },
    setItem: function (key, data) {
      localStorage.setItem(key, data);
    },
    setObject: function (key, data) {
      localStorage.setItem(key, angular.toJson(data));
    },
    remove: function (key) {
      localStorage.removeItem(key);
    },
    clearAll : function () {
      localStorage.clear();
    }
  };
});
// Einde
