// Copyright Jaap Harmsma & Rob Hopster 2016-2017 **Just Add Sugar B.V.**
// info@justaddsugar.nl of http://www.justaddsugar.nl


angular.module('starter.controllers', [])


.service('UserService', function() {
  var setUser = function(user_data) {
    window.localStorage.starter_facebook_user = JSON.stringify(user_data);
  };

  var getUser = function(){
    return JSON.parse(window.localStorage.starter_facebook_user || '{}');
  };

  return {
    getUser: getUser,
    setUser: setUser
  };
})


/*
.controller('ActivateRequestCtrl', function($window, ApiEndpoint, LadiesService, $scope, $rootScope, $localForage, $ionicScrollDelegate, $ionicPlatform, $cordovaSQLite, $ionicViewSwitcher, ActivateService, Storage, $state, ionicToast, $ionicLoading, $ionicPopover) {
	$scope.active = {email:"", deviceid:""};
        
  var template = '<ion-popover-view><ion-header-bar> <h1 class="title">Alleen voor leden!</h1> </ion-header-bar> <ion-content> Dit is het e-mail adres welke bekend is bij de ledenadministratie van Ladies Circle Nederland. Indien je een e-mail adres gebruikt die hier niet bekend is werkt deze applicatie niet. </ion-content></ion-popover-view>';

  $scope.popover = $ionicPopover.fromTemplate(template, {
    scope: $scope
  });
  $scope.openPopover = function($event) {
    $scope.popover.show($event);
  };
  $scope.closePopover = function() {
    $scope.popover.hide();
  };
  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });
  $scope.$on('popover.hidden', function() {
  });
  $scope.$on('popover.removed', function() {
  });
 
       var emailn =  "";//JSON.stringify($localForage.getItem('setedemail')); //Storage.getItem('setedemail');
       var passn = "";//Storage.getItem('setpassword');
*/

/*copy of NTR from here */
.controller("LoginCtrl", function($window, ApiEndpoint, LadiesService, $scope, $rootScope, $localForage, $ionicScrollDelegate, $ionicPlatform, $cordovaSQLite, $ionicViewSwitcher, ActivateService, Storage, $state, ionicToast, $ionicLoading, $ionicPopover) {
    $scope.current = {
        tab: "inloggen"
    };
    $scope.changeTab = function (tabname) {
        //  $ionicScrollDelegate.resize();
        $scope.current.tab = tabname;
    };

    $scope.active = {
        email: "",
        deviceid: ""
    };
    ga_storage._trackEvent("Welk screen?", "Inloggen");

    var template = '<ion-popover-view><ion-header-bar> <h1 class="title">Meer info</h1> </ion-header-bar> <ion-content><p style="padding-left:10px; padding-right:10px;">Dit is het e-mail adres waar je b.v. ook de nieuwsbrieven van de Ladies Circle op ontvangt, en ook het e-mail adres dat andere gebruikers van LAPP kunnen zien op je profiel pagina. </p></ion-content></ion-popover-view>';

    $scope.popover = $ionicPopover.fromTemplate(template, {
        scope: $scope
    });

    $scope.openPopover = function ($event) {
        $scope.popover.show($event);
    };

    $scope.closePopover = function () {
        $scope.popover.hide();
    };

    $scope.$on('$destroy', function () {
        $scope.popover.remove();
    });

    $scope.$on('popover.hidden', function () {

    });

    $scope.$on('popover.removed', function () {

    });

    var emailn = ""; //JSON.stringify($localForage.getItem('setedemail')); //Storage.getItem('setedemail');
    var passn = ""; //Storage.getItem('setpassword');

/*copy of NTR to here */
        
        $localForage.getItem('setedemail').then(function(data) {
            if(data)
            {
                Storage.setItem("setedemail",data);
                emailn = data;
                 $localForage.getItem('setpassword').then(function(data) {
            if(data)
            {
                Storage.setItem("setpassword",data);
                passn = data;
                if(emailn && passn ) 
                {
                  Storage.setObject("activeUser", {email:emailn, password:passn});
		
                    $ionicViewSwitcher.nextDirection('forward');
                    $state.go("app.home");
                }
	
            }
                });
            }
        });
        
        $localForage.getItem('device_ID').then(function(data) {
            if(data)
            {
                Storage.setItem("device_ID",data);
            }
        });

		$scope.checkIfEmailAndPasswordIsKnownByMas = function(){
		if(!$scope.active || $scope.active.email == "" || $scope.active.email == undefined)
		{
                        $scope.active.email = '';
                        $scope.active.password = '';
			ionicToast.show("Je emailadres is helaas niet geldig. probeer het nogmaals met een geldig emailadres", 'bottom', false, 4000);
			$state.go("login");
			return;
		}

		if($scope.active.password == "")
		{
                        $scope.active.email = '';
                        $scope.active.password = '';

			ionicToast.show("Je wachtwoord is niet geldig", 'bottom', false, 4000);
			return;
		}

                var random, deviceID;
	    random = new Random(Random.engines.browserCrypto);
	    deviceID = random.uint32();
		console.log(deviceID);
		$scope.active.deviceid = deviceID;

		$ionicLoading.show();
		var promise = ActivateService.checkIfMemberExistInMas({email:$scope.active.email, password:$scope.active.password});
  		promise.then(function(result){

                    if(result.requests_left == "0")
                     {
                         ionicToast.show("U heeft voor vandaag helaas het maximum aantal LAPP requests verbruikt in het ledenadministratiesysteem van Ladies Circle Nederland.", 'bottom', false, 4000);
                     }

			console.log(result);

			if(result.result == "ok"){
                                
                                $localForage.setItem('setedemail',$scope.active.email).then(function() {
                                });    
                               $localForage.setItem('setpassword',$scope.active.password).then(function() {
                                });
                               $localForage.setItem('device_ID',$scope.active.deviceid).then(function() {
                                });
                                
                                Storage.setItem("setedemail",$scope.active.email);
                                Storage.setItem("setpassword",$scope.active.password);
                                Storage.setItem("device_ID",$scope.active.deviceid);
				Storage.setObject("activeUser", {email:$scope.active.email, password:$scope.active.password});
				if(Storage.getItem("firstSlider") && Storage.getItem("firstSlider") == 'true'){
					$ionicViewSwitcher.nextDirection('forward');
					$state.go("app.home");
				}else{
					$ionicViewSwitcher.nextDirection('forward');
					$state.go("slider");
				}
			}else{

				ionicToast.show("Helaas, deze gegevens zijn niet bekend in het LAS", 'bottom', false, 4000);
			}
			$ionicLoading.hide();
  		}, function(reason){
			console.log(reason);
			$ionicLoading.hide();
  		});

		return;

	};

})

.controller('ActivateEnterCtrl', function($scope,$ionicViewSwitcher,$localForage,$cordovaSQLite, $rootScope,$ionicPlatform, $state, ionicToast, Storage, $ionicLoading, ActivateService, Storage) {
	$scope.goToHelp = function(){
		$state.go("activate.help");
	};

	$scope.active = {password:""};
	//$scope.active = {password:""};


		$scope.checkIfEmailIsKnownByMas = function(){

		if ($scope.active.email == "" || $scope.active.email == undefined) {
			ionicToast.show("Je emailadres is helaas niet geldig. probeer het nogmaals met een geldig emailadres", 'bottom', false, 4000);
            return;
        }

		// $rootScope.active = {email:$scope.active.email};

	    var random, deviceID;
	    // random = new Random(Random.engines.mt19937().autoSeed());
	    random = new Random(Random.engines.browserCrypto);
	    deviceID = random.uint32();
		console.log(deviceID);
		$scope.active.deviceid = deviceID;

		$ionicLoading.show();

		var promise = ActivateService.activeEmail($scope.active);
  		promise.then(function(result){
			console.log(result);

			if(result.reason == "unknown C"){
				$rootScope.active = {email:$scope.active.email, deviceid:$scope.active.deviceid};
				ionicToast.show("Dankjewel. We sturen je een wachtwoord toe.", 'bottom', false, 4000);
				$state.go("login");
			}else{
				ionicToast.show("We hebben geen geldige informatie ontvangen. Activatie mislukt.", 'bottom', false, 4000);
			}
			$ionicLoading.hide();
  		}, function(reason){
			console.log(reason);
			$ionicLoading.hide();
  		});
	};
})

/*
.controller('ActivateHelpCtrl', function($scope, $state, $cordovaSocialSharing, $ionicViewSwitcher, $stateParams,$ionicPopover) {

// .fromTemplate() method
  var template = '<ion-popover-view><ion-header-bar> <h1 class="title">My Popover Title</h1> </ion-header-bar> <ion-content> Hello! </ion-content></ion-popover-view>';

  $scope.popover = $ionicPopover.fromTemplate(template, {
    scope: $scope
  });

  // .fromTemplateUrl() method
  $ionicPopover.fromTemplateUrl('my-popover.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });


  $scope.openPopover = function($event) {
    $scope.popover.show($event);
  };
  $scope.closePopover = function() {
    $scope.popover.hide();
  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });
  // Execute action on hide popover
  $scope.$on('popover.hidden', function() {
    // Execute action
  });
  // Execute action on remove popover
  $scope.$on('popover.removed', function() {
    // Execute action
  });

      $scope.onEmail = function(){
		$cordovaSocialSharing.shareViaEmail("", "Inlogprobleem LAPP v2.0", "app@ladiescircle.nl" , "", "", null).then(function(result) {
		  // Success!
		}, function(err) {
		  // An error occurred. Show a message to the user
		});

	  };
})
*/
.controller('SliderCtrl', function($scope, $state, $ionicSlideBoxDelegate, Storage) {
	$scope.prev = function(){
		$ionicSlideBoxDelegate.previous();
	};

	$scope.changeButton = false;
	$scope.next = function(){
		if($ionicSlideBoxDelegate.currentIndex() == 7){
			$state.go("app.home");
			$ionicSlideBoxDelegate.slide(0);
			return;
		}

		if($ionicSlideBoxDelegate.currentIndex() == 6){
			$scope.changeButton = true;
		}

		$ionicSlideBoxDelegate.next();
	};

	$scope.slideHasChanged = function($index){
		if($index !== 0) {
		  $scope.first = true;
		} else {
		  $scope.first = false;
		}

		if($index == 7){
			Storage.setItem("firstSlider", true);
			$state.go("app.home");
			$scope.changeButton = false;
			$ionicSlideBoxDelegate.slide(0);
			return;
		}

		if($index == 6){
			$scope.changeButton = true;
		}
	};

})

.controller('PreLoadingCtrl', function($scope, $state, $timeout, $ionicViewSwitcher, $ionicHistory) {
	$state.go("app.home");
})

.controller('AppCtrl', function($scope, $state, Storage,$localForage, ApiEndpoint, $ionicViewSwitcher, $ionicLoading, ActivateService, $cordovaInAppBrowser, $ionicHistory, $ionicSideMenuDelegate) {
	$scope.activeUser = Storage.getObject("activeUser");
	console.log($scope.activeUser);

	$scope.logout = function(){
                  Storage.setItem("setedemail",'');
                Storage.setItem("setpassword",'');
                Storage.setObject("activeUser", {email:'', password:''});
                $localForage.setItem('setedemail','').then(function() {
                                });    
                $localForage.setItem('setpassword','').then(function() {
                                });
                $localForage.setItem('device_ID','').then(function() {
                                });                
                           
		$state.go("login");
	};

	$scope.goSlider = function(){
		$state.go("slider");
	};

	$scope.goBack = function(){
		$ionicHistory.goBack();
	};

	$scope.toggleMenu = function(){
		$ionicSideMenuDelegate.toggleLeft();
	};

	$scope.goCirclerDetailEdit = function(){
			$state.go("app.Circler_detail_edit", {"selectedItem":angular.toJson($scope.myInformation)});
	};

	$scope.goCirclerDetail = function(){
			$state.go("app.Circler_detail", {"selectedItem":angular.toJson($scope.myInformation)});
	};

	$scope.goSearch = function(){
		$state.go("app.search");
	};

	var defaultOptions = {
		"location":"yes",
		"clearcache":"no",
		"toolbar":"yes"
	};

	$scope.onWebshop = function(){
		$cordovaInAppBrowser.open("http://shop.roundCircle.nl", '_blank', defaultOptions).then(function(event){
		}).catch(function(event){
		});
	};

	$scope.onMagazine = function(){
		$cordovaInAppBrowser.open("https://issuu.com/prodacosta", '_blank', defaultOptions).then(function(event){
		}).catch(function(event){
		});
	};


	$scope.onAgm = function(){
		$state.go("app.articles", {"selectedItem":angular.toJson({"page-LAS-id": "21", "page-shortname": "Jaarlijkse AGM"})});
	};

	$scope.onHalf = function(){
		$state.go("app.articles", {"selectedItem":angular.toJson({"page-LAS-id": "23", "page-shortname": "Halfjaarlijkse"})});
	};

	$scope.onCtm = function(){
		$state.go("app.articles", {"selectedItem":angular.toJson({"page-LAS-id": "26", "page-shortname": "Jaarlijkse CTM"})});
	};

	$scope.onOverTapp = function(){
		$state.go("app.articles", {"selectedItem":angular.toJson({"page-LAS-id": "64", "page-shortname": "Over deze (L)APP"})});
	};

	$scope.myInformation = "";
	var getCurrentUserDetails = function(){
		$ionicLoading.show();

		var promise = ActivateService.getCurrentUserDetails($scope.activeUser);
  		promise.then(function(result){

			if(result.data){
				  if(result.data[0].photo_last_modification_date != '')
                                                     {
                        result.data[0].photo = ApiEndpoint.url+"/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[0].member_id;
				 }else{
                                                         result.data[0].photo = 'no image';
                                                    }

                        $scope.myInformation = result.data[0];
			}

			console.log($scope.myInformation);

			$ionicLoading.hide();
  		}, function(reason){
			console.log(reason);
			$ionicLoading.hide();
  		});

	};
	getCurrentUserDetails();

})


.controller('HomeCtrl', function($scope, $state, $timeout, Storage, ApiEndpoint, $ionicLoading, $ionicScrollDelegate, CircleService, AgendaService, NTRService, LadiesService, NewsService, ionicToast, $cordovaGoogleAnalytics) {
	$scope.activeUser = Storage.getObject("activeUser");
	console.log($scope.activeUser);

	  $scope.graph = {};
	  $scope.graph.data = [
		//Awake
		[16, 45, 20, 32, 16, 30, 23],

	  ];
	  $scope.graph.labels = ['2010', '2011', '2012', '2013', '2014', '2015', '2016'];
	  $scope.graph.series = ['Awake'];

	  $scope.current = { tab:"dashboard"};
	  $scope.changeTab = function(tabname){
		$ionicScrollDelegate.resize();
		$scope.current.tab = tabname;
	  }

	var rootDefs = [];
	$scope.newestCirclers = new Array();
	$scope.newsList = new Array();
	$scope.agendaList = new Array();
	$scope.ntrList = new Array();
	$scope.CircleList = new Array();
	$scope.districtList = new Array();

	var getNewestCirclers = function(){
		var def = $.Deferred();

		var promise = LadiesService.GetNewestMembersHavingAPicture($scope.activeUser);
  		promise.then(function(result){
                       if(result.requests_left == "1")
                       {
                           ionicToast.show("U heeft voor vandaag helaas het maximum aantal LAPP requests verbruikt in het ledenadministratiesysteem van Ladies Circle Nederland.", 'bottom', false, 4000);
                       }
			if(result.data){
				for(var index in result.data){

                                    if(index < 4)

                                {
				  //Yashwant
                                        if(result.data[index].photo_last_modification_date != '')
                                            {

                                                result.data[index].photo = "https://lcnl.interleave.nl/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[index].member_id;

                                       }else{
                                             result.data[index].photo = 'no image';
                                        } //
                                        //
                             $scope.newestCirclers.push(result.data[index]);

                                }
                                //
				}
			}
			console.log($scope.newestCirclers);
			def.resolve(1);
  		}, function(reason){
			console.log(reason);
			def.resolve(0);
  		});

		rootDefs.push(def.promise());
	};

		$scope.newsList = new Array();
		var getNews = function(){
		var def = $.Deferred();
		$ionicLoading.show();

		var promise = NewsService.all($scope.activeUser);
  		promise.then(function(result){
			console.log(result);
			if(result.posts){
				for(var index in result.posts){
					if(index < 5)
                                        {
                                            
						$scope.newsList.push(result.posts[index]);
                                                
                                                $scope.newsList[index].content = $scope.newsList[index].content.replace(/<[^>]+>/gm, '');
                                            
                                                
                                        }

				}
			}
			console.log($scope.newsList);
			def.resolve(1);
  		}, function(reason){
			console.log(reason);
			def.resolve(0);
  		});

		rootDefs.push(def.promise());
	};

	var getAgenda = function(){
		var def = $.Deferred();

		var promise = AgendaService.all($scope.activeUser);
  		promise.then(function(result){

			if(result.data){
				for(var index in result.data){
					if(index < 5)
					{
						result.data[index].date = moment(result.data[index].startdatetime + "").lang('nl').format("D");
						result.data[index].month = moment(result.data[index].startdatetime + "").lang('nl').format("MMMM");
						result.data[index].year = moment(result.data[index].startdatetime + "").lang('nl').format("YYYY");
						$scope.agendaList.push(result.data[index]);
					}
				}
			}
			console.log($scope.agendaList);
			def.resolve(1);
  		}, function(reason){
			console.log(reason);
			def.resolve(0);
  		});

		rootDefs.push(def.promise());
	};

	var getNTR = function(){
		var def = $.Deferred();

		var promise = NTRService.detailNTR($scope.activeUser);
  		promise.then(function(result){

			if(result.data){
				for(var index in result.data){
					if(index < 4)
					{
						$scope.ntrList.push(result.data[index]);
					}
				}
			}
			console.log($scope.ntrList[0]);

			  $scope.graph = {};
			  $scope.graph.data = [
				//Awake
				[$scope.ntrList[0]["#circlers3jaargeleden"],
					$scope.ntrList[0]["#circlers2jaargeleden"],
					$scope.ntrList[0]["#circlers1jaargeleden"],
					$scope.ntrList[0]["#circlers"],
					$scope.ntrList[0]["#circlers1jaarindetoekomst"]],
					//$scope.ntrList[0]["#circlers2jaarindetoekomst"]],

			  ];
			  $scope.graph.labels = ['2013', '2014', '2015', '2016', 'Huidig'];
			  $scope.graph.series = ['Awake'];

			def.resolve(1);
  		}, function(reason){
			console.log(reason);
			def.resolve(0);
  		});

		rootDefs.push(def.promise());
	};


	var getCircles = function(){
		var def = $.Deferred();
		var promise = CircleService.all($scope.activeUser);
  		promise.then(function(result){

			if(result.data){
                        for(var index in result.data){
					result.data[index].date = moment(result.data[index].charter_date + "").lang('nl').format("D MMMM YYYY");
					$scope.CircleList.push(result.data[index]);

					var flag = false;
					for(var i in $scope.districtList){
						if($scope.districtList[i].id == result.data[index].circle){
							flag = true;
						}
					}

					if(flag == false){
						$scope.districtList.push({id:parseInt(result.data[index].circle), name:result.data[index].circle, Circle:[]});
					}
				}

				for(var z in $scope.districtList){
					for(var y in result.data){
						if($scope.districtList[z].id == $scope.CircleList[y].circle)
                                                {
							$scope.districtList[z].Circle.push($scope.CircleList[y]);

                                                }
					}
				}

			}
			try{
			$scope.districtList[14].id = 17;
			}catch(e){console.log(e);}

			console.log($scope.districtList);
			def.resolve(1);
  		}, function(reason){
			console.log(reason);
			def.resolve(0);
  		});

		rootDefs.push(def.promise());
	};

	var getNationalDashboardData = function(){
		$ionicLoading.show();

		rootDefs = [];
		getNewestCirclers();
		getNews();
		getAgenda();
		getNTR();
		getCircles();

		$.when.apply($, rootDefs).then(function() {
			console.log("all things done");
			$ionicLoading.hide();
		});
	};
	getNationalDashboardData();

	  $scope.goNewsDetail = function(index){
		$state.go("app.news_detail", {"selectedItem":angular.toJson($scope.newsList[index])});
	  };

	  $scope.goAgendaDetail = function(index){
		$state.go("app.agenda_detail", {"selectedItem":angular.toJson($scope.agendaList[index])});
	  };

	  $scope.goCirclerDetail = function(index){
		  $state.go("app.Circler_detail", {"selectedItem":angular.toJson($scope.newestCirclers[index])});
	  };

	  $scope.sCirclerDetail = function(index){
		  $state.go("app.Circler_detail", {"selectedItem":angular.toJson($scope.searchList[index])});
	  };

	  $scope.sCircleDetail = function(index){
		  $state.go("app.Circle_detail", {"selectedItem":angular.toJson($scope.searchList[index])});
	  };

	  $scope.goCircleDetail = function(index){
		  $state.go("app.Circle_detail", {"selectedItem":angular.toJson($scope.CircleList[index])});
	  };

	  $scope.goDistrictDetail = function(item){
		  $state.go("app.district_detail", {"selectedItem":angular.toJson(item)});
	  };

	  $scope.goSlider = function(){
		$state.go("slider");
		};

	  $scope.goAgenda = function(){
		$state.go("app.agenda");
	  };


	  $scope.searchOption = {
		Circlers:true,
		Circle:false,
		text:""
	  }

	  $scope.searchList = new Array();
	  $scope.onSearch = function(){
			if($scope.searchOption.text == ""){
				ionicToast.show("Voer een zoekterm in", 'bottom', false, 3000);
				return;
			}

			$scope.searchList = new Array();
			console.log($scope.searchOption);
			$ionicLoading.show();

			if($scope.searchOption.Circlers == true){
				var _param = {
					email:$scope.activeUser.email,
					password:$scope.activeUser.password,
					searchWord:$scope.searchOption.text
				};

				var promise = LadiesService.SearchCirclers(_param);
				promise.then(function(result){

					if(result.data){
						for(var index in result.data){
				 if(result.data[index].photo_last_modification_date != '')
                                {

                                   result.data[index].photo = ApiEndpoint.url+"/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[index].member_id;
                        }else{
                            result.data[index].photo = 'no image';
                        }

                                            $scope.searchList.push(result.data[index]);
						}
					}
					console.log($scope.searchList);
					$ionicLoading.hide();
				}, function(reason){
					console.log(reason);
					$ionicLoading.hide();
				});
			}else{
				var _param = {
					email:$scope.activeUser.email,
					password:$scope.activeUser.password,
					searchWord:$scope.searchOption.text
				};

				var promise = LadiesService.SearchCircle(_param);
				promise.then(function(result){

					if(result.data){
						for(var index in result.data){
							$scope.searchList.push(result.data[index]);
						}
					}
					console.log($scope.searchList);
					$ionicLoading.hide();
				}, function(reason){
					console.log(reason);
					$ionicLoading.hide();
				});
			}
	  };

	  $scope.changeCirclers = function(){
		  $scope.searchList = new Array();

		if($scope.searchOption.Circlers == true){
			$scope.searchOption.Circle = false;
		}else{
			$scope.searchOption.Circle = true;
		}
	  };

	  $scope.changeCircles = function(){
		  $scope.searchList = new Array();
		if($scope.searchOption.Circle == true){
			$scope.searchOption.Circlers = false;
		}else{
			$scope.searchOption.Circlers = true;
		}
	  };
})

.controller('AgendaCtrl', function($scope, $state, $http, Storage,$window, AgendaService, $stateParams, $ionicLoading, $ionicScrollDelegate, $cordovaSocialSharing, $cordovaCalendar, $cordovaInAppBrowser, ionicToast) {
	  $scope.current = { tab:"Circle"};
      $scope.visibleplusicon = '1';
	  $scope.changeTab = function(tabname){
		  $ionicScrollDelegate.resize();
		  $scope.current.tab = tabname;

                  $scope.visibleplusicon = '2';
	  }

	  $scope.goMyCircleAgendaDetail = function(index){
              
		$state.go("app.agenda_detail", {"selectedItem":angular.toJson($scope.myCircle[index])});
	  };

	  $scope.goNationalAgendaDetail = function(index){
		$state.go("app.agenda_detail", {"selectedItem":angular.toJson($scope.national[index])});
	  };

	  	$scope.goSlider = function(){
		$state.go("slider");
		};

	  $scope.activeUser = Storage.getObject("activeUser");
	  console.log($scope.activeUser);

	var rootDefs = [];
	$scope.myCircle = new Array();
	$scope.myDistrict = new Array();
	$scope.national = new Array();

	var getMyCircleAgenda = function(){
		var def = $.Deferred();

		var promise = AgendaService.myCircleAgenda($scope.activeUser);
  		promise.then(function(result){
	          if(result.data){
                       if(result.requests_left == "1")
                       {
                           ionicToast.show("U heeft voor vandaag helaas het maximum aantal LAPP requests verbruikt in het ledenadministratiesysteem van Ladies Circle Nederland.", 'bottom', false, 4000);
                       }
                        
                           // alert(JSON.stringify(result.data));
                            console.log(JSON.stringify(result.data));
				for(var index in result.data){
					result.data[index].date = moment(result.data[index].startdatetime + "").lang('nl').format("D");
					result.data[index].month = moment(result.data[index].startdatetime + "").lang('nl').format("MMMM");
					result.data[index].year = moment(result.data[index].startdatetime + "").lang('nl').format("YYYY");
					$scope.myCircle.push(result.data[index]);
				}
			}

			console.log(JSON.stringify($scope.myCircle));
			def.resolve(1);
  		}, function(reason){
			console.log(reason);
			def.resolve(0);
  		});

		rootDefs.push(def.promise());
	};


	var getMyDistrictAgenda = function(){
		var def = $.Deferred();

		var promise = AgendaService.myDistrictAgenda($scope.activeUser);
  		promise.then(function(result){
			if(result.data){
				for(var index in result.data){
					result.data[index].date = moment(result.data[index].startdatetime + "").lang('nl').format("D");
					result.data[index].month = moment(result.data[index].startdatetime + "").lang('nl').format("MMMM");
					result.data[index].year = moment(result.data[index].startdatetime + "").lang('nl').format("YYYY");
					$scope.myDistrict.push(result.data[index]);
				}
			}

			console.log(JSON.stringify($scope.myDistrict));
			def.resolve(1);
  		}, function(reason){
			console.log(reason);
			def.resolve(0);
  		});

		rootDefs.push(def.promise());
	};

	var getNationalAgenda = function(){
		var def = $.Deferred();

		var promise = AgendaService.all($scope.activeUser);
  		promise.then(function(result){

			if(result.data){
				for(var index in result.data){
					result.data[index].date = moment(result.data[index].startdatetime + "").lang('nl').format("D");
					result.data[index].month = moment(result.data[index].startdatetime + "").lang('nl').format("MMMM");
					result.data[index].year = moment(result.data[index].startdatetime + "").lang('nl').format("YYYY");
					$scope.national.push(result.data[index]);
				}
			}

			console.log(JSON.stringify($scope.national));
			def.resolve(1);
  		}, function(reason){
			console.log(reason);
			def.resolve(0);
  		});

		rootDefs.push(def.promise());
	};

	var getAgendaData = function(){
		$ionicLoading.show();

		rootDefs = [];
		$scope.myCircle = new Array();
		$scope.myDistrict = new Array();
		$scope.national = new Array();

		getMyCircleAgenda();
		getMyDistrictAgenda();
		getNationalAgenda();

		$.when.apply($, rootDefs).then(function() {
			console.log("all things done");
			$ionicLoading.hide();
		});
	};
	getAgendaData();

	var promise = AgendaService.myIcalimport($scope.activeUser);
  		promise.then(function(response){
			if(response.data){

                    console.log(JSON.stringify(response.data));

                      $scope.finalurl = response.data.iCalsubscribedcalendarURL+'&username='+$scope.activeUser.email+'&password='+$scope.activeUser.password;

                        }
                    });


         $scope.onIcal = function(){

              getAgendaData1();

	};

        // export Icall //

	$scope.addNewAgenda = function(){
		$state.go("app.addagenda");
	};

	$scope.goSlider = function(){
		$state.go("slider");
	};
        
        
      $scope.openWindow = function(){
          
                  //alert('Hello');
           $window.open($scope.finalurl,'_system');
           
           
      };    

})



.controller('AddagendaCtrl', function($scope, $state, $http, Storage, AgendaService, $stateParams, $ionicLoading, $ionicScrollDelegate, $cordovaSocialSharing, $cordovaCalendar, $cordovaInAppBrowser, ionicToast) {
$scope.activeUser = Storage.getObject("activeUser");
//alert(JSON.stringify($scope.activeUser));


      //  $scope.datetime=new Date();
        
         $scope.dateExample = {
         value: new Date(2017, 6, 1, 19, 00),
         value1: new Date(2017, 6, 2, 19, 00)
       };
        
	$scope.setting = {
		alert_pro:false
	};


$scope.programs = ["Gezelligheid", "Service / goed doel", "Regionaal Circlen","Nationaal Circlen","Internationaal Circlen","Vergadering"];


      $scope.changeAlert = function(){
       
          if($scope.setting.alert_pro == true)
            {
                $scope.setting.alert_pro = false;
            }else{
                $scope.setting.alert_pro = true;
            }
      };


      $scope.onSave = function(selectedCircler){
          
          $scope.selectedCircler = selectedCircler;

        //  alert($scope.selectedCircler.startdatetime);
            var alert_p="";
            if ($scope.setting.alert_pro == true)
            {
                alert_p = "Ja";
            }else{
                alert_p = "Nee";
            }
                console.log($scope.selectedCircler);

		$ionicLoading.show();
                
               // alert(d1);
		var agendaobj = {
                type:selectedCircler.type,
                startdatetime: $scope.dateExample.value,
                enddatetime: $scope.dateExample.value1,
                description: selectedCircler.description,
                location: selectedCircler.location,
                remarks: selectedCircler.remarks,
            	buttontext: selectedCircler.buttontext,
                alert_pro:alert_p
       };

   //alert (JSON.stringify(agendaobj));


       var _param ={
                        email:$scope.activeUser.email,
			password:$scope.activeUser.password,
			data:agendaobj
	   }

           
		var promise = AgendaService.AddAgenda(_param);
  		promise.then(function(result){
			console.log(result);
			$ionicLoading.hide();
                        //alert (JSON.stringify(result));

			if(result.result == "nok"){
				ionicToast.show(result.reason, 'bottom', false, 3000);
			}else{
				ionicToast.show("Your Agenda has been Added successfully", 'bottom', false, 3000);
                              //  $state.go("app.agenda");
			}
  		}, function(reason){
			console.log(reason);
			$ionicLoading.hide();

  		});

  			  $scope.goSlider = function(){
		$state.go("slider");
		};
	};
        
        
        
})

.controller('SearchCtrl', function($scope, $state, ApiEndpoint, Storage, LadiesService, $ionicLoading) {
	$scope.activeUser = Storage.getObject("activeUser");

	  $scope.searchList = new Array();
		$scope.onSearch = function(){
			if($scope.searchOption.text == ""){
				ionicToast.show("Voer een zoekterm in", 'bottom', false, 3000);
				return;
			}

			$scope.searchList = new Array();
			console.log($scope.searchOption);
			$ionicLoading.show();

			if($scope.searchOption.Circlers == true){
				var _param = {
					email:$scope.activeUser.email,
					password:$scope.activeUser.password,
					searchWord:$scope.searchOption.text
				};

				var promise = LadiesService.SearchCirclers(_param);
				promise.then(function(result){

					if(result.data){
                                            
                                             if(result.requests_left == "1")
                                                {
                                                    ionicToast.show("U heeft voor vandaag helaas het maximum aantal LAPP requests verbruikt in het ledenadministratiesysteem van Ladies Circle Nederland.", 'bottom', false, 4000);
                                                }
                                        
                                            
                                          	for(var index in result.data){
							  if(result.data[index].photo_last_modification_date != '')
                        {
                                            result.data[index].photo = ApiEndpoint.url+"/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[index].member_id;
							 }else{
                            result.data[index].photo = 'no image';
                        }
                                            $scope.searchList.push(result.data[index]);
						}
					}
					console.log($scope.searchList);
					$ionicLoading.hide();
				}, function(reason){
					console.log(reason);
					$ionicLoading.hide();
				});
			}else{
				var _param = {
					email:$scope.activeUser.email,
					password:$scope.activeUser.password,
					searchWord:$scope.searchOption.text
				};

				var promise = LadiesService.SearchCircle(_param);
				promise.then(function(result){
					//  alert (JSON.stringify(result.data));
					if(result.data){
                                          if(result.requests_left == "1")
                                                {
                                                    ionicToast.show("U heeft voor vandaag helaas het maximum aantal LAPP requests verbruikt in het ledenadministratiesysteem van Ladies Circle Nederland.", 'bottom', false, 4000);
                                                }
						for(var index in result.data){
                                                    //if ((result.data[index].name == $scope.searchOption.text) || (result.data[index].Circle == $scope.searchOption.text))
                                                    if ((result.data[index].name.toLocaleLowerCase().indexOf($scope.searchOption.text.toLocaleLowerCase()) > -1) || (result.data[index].circle.toLocaleLowerCase().indexOf($scope.searchOption.text.toLocaleLowerCase()) > -1))
							$scope.searchList.push(result.data[index]);
						}
					}
					console.log($scope.searchList);
					$ionicLoading.hide();
				}, function(reason){
					console.log(reason);
					$ionicLoading.hide();
				});
			}
	  };

	  $scope.searchOption = { Circle:false, Circlers:true};
	  $scope.changeCirclers = function(){
		  $scope.searchList = new Array();

		if($scope.searchOption.Circlers == true){
			$scope.searchOption.Circle = false;
		}else{
			$scope.searchOption.Circle = true;
		}
	  };

	  $scope.changeCircles = function(){
		  $scope.searchList = new Array();
		if($scope.searchOption.Circle == true){
			$scope.searchOption.Circlers = false;
		}else{
			$scope.searchOption.Circlers = true;
		}
	  };

	  $scope.sCirclerDetail = function(index){
		  $state.go("app.Circler_detail", {"selectedItem":angular.toJson($scope.searchList[index])});
	  };

	  $scope.goSlider = function(){
		$state.go("slider");
		};

	  $scope.sCircleDetail = function(index){
		  $state.go("app.Circle_detail", {"selectedItem":angular.toJson($scope.searchList[index])});
	  };
})

.controller('DistrictDetailCtrl', function($scope, $state, ApiEndpoint, $stateParams,Storage,  LadiesService, $ionicLoading) {
	$scope.activeUser = Storage.getObject("activeUser");
	$scope.selectedDistrict = angular.fromJson($stateParams.selectedItem);
	console.log($scope.selectedDistrict);
	$scope.CircleList = $scope.selectedDistrict.Circle;

	$scope.goCircleDetail = function(item){
		  $state.go("app.Circle_detail", {"selectedItem":angular.toJson(item)});
	};

	$scope.goSlider = function(){
		$state.go("slider");
		};
})

.controller('CircleDetailCtrl', function($scope, $state, ApiEndpoint, $stateParams,Storage, LadiesService, $ionicLoading, $window) {
	$scope.activeUser = Storage.getObject("activeUser");
	$scope.selectedCircler = angular.fromJson($stateParams.selectedItem);

        //alert(JSON.stringify($scope.selectedCircler.Circle));
        $scope.activeUser.tbl=$scope.selectedCircler.Circle;
        $scope.roleinCircle1 = "";
        $scope.roleinCircle2 = "";
        $scope.roleinCircle3 = "";
        $scope.roleinCircle4 = "";
        $scope.roleinCircle5 = "";

	console.log($scope.selectedCircler);



	$scope.circlers = new Array();
	var getMembersOfCirclers = function(){
		$ionicLoading.show();

		var promise = LadiesService.getMembersOfCircle($scope.activeUser);
  		promise.then(function(result){

			if(result.data){
                            
                            if(result.requests_left == "1")
                            {
                                ionicToast.show("U heeft voor vandaag helaas het maximum aantal LAPP requests verbruikt in het ledenadministratiesysteem van Ladies Circle Nederland.", 'bottom', false, 4000);
                            }

				for(var index in result.data){
					if($scope.selectedCircler.circle == result.data[index].circle)
					{
						result.data[index].photo = ApiEndpoint.url+"/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[index].member_id;

                                                $scope.circlers.push(result.data[index]);

                                                 if ( result.data[index].role_in_circle == 'Voorzitter')
                                                    {

                                                     $scope.roleinCircle1 = result.data[index];
                                                     $scope.roleinCircle1.ind=index;
                                                     if(result.data[index].photo_last_modification_date != '')
                                                     {
                                                                $scope.roleinCircle1.photo = ApiEndpoint.url+"/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[index].member_id;
                                                     }else{
                                                                   $scope.roleinCircle1.photo = 'no image';
                                                  }
                                                 }
                                                  if ( result.data[index].role_in_circle == 'Secretaris')
                                                    {

                                                     $scope.roleinCircle2 = result.data[index];
                                                     $scope.roleinCircle2.ind1=index;
                                                     if(result.data[index].photo_last_modification_date != '')
                                                     {
                                                                $scope.roleinCircle2.photo = ApiEndpoint.url+"/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[index].member_id;
                                                     }else{
                                                                   $scope.roleinCircle2.photo = 'no image';
                                                  }
                                                 }
                                                 if ( result.data[index].role_in_circle == 'Penningmeester')
                                                    {

                                                     $scope.roleinCircle3 = result.data[index];
                                                        $scope.roleinCircle3.ind2=index;
                                                     if(result.data[index].photo_last_modification_date != '')
                                                     {
                                                                $scope.roleinCircle3.photo = ApiEndpoint.url+"/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[index].member_id;
                                                     }else{
                                                                   $scope.roleinCircle3.photo = 'no image';
                                                  }
                                                 }
                                                 if ( result.data[index].role_in_circle == 'IRO')
                                                    {

                                                     $scope.roleinCircle4 = result.data[index];
                                                     $scope.roleinCircle4.ind3=index;
                                                     if(result.data[index].photo_last_modification_date != '')
                                                     {
                                                                $scope.roleinCircle4.photo = ApiEndpoint.url+"/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[index].member_id;
                                                     }else{
                                                                   $scope.roleinCircle4.photo = 'no image';
                                                  }
                                                 }
                                                 if ( result.data[index].role_in_circle == 'PRO')
                                                    {

                                                     $scope.roleinCircle5 = result.data[index];
                                                     $scope.roleinCircle5.ind4=index;
                                                     if(result.data[index].photo_last_modification_date != '')
                                                     {
                                                                $scope.roleinCircle5.photo = ApiEndpoint.url+"/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[index].member_id;
                                                     }else{
                                                                   $scope.roleinCircle5.photo = 'no image';
                                                  }
                                                 }
					}

				}

			}
			console.log($scope.circlers);
			$ionicLoading.hide();
  		}, function(reason){
			console.log(reason);
			$ionicLoading.hide();
  		});
	};

        getMembersOfCirclers();

	$scope.goCirclerDetail = function(index){
		$state.go("app.circler_detail", {"selectedItem":angular.toJson($scope.circlers[index])});
	};

       $scope.goLadies = function(index){
		  $state.go("app.ladies", {"selectedItem":angular.toJson($scope.selectedCircler)});
	  };
          
       $scope.websiteClick = function(index){
		$window.open($scope.selectedCircler.website,'_system');
	};
        
        $scope.facebookClick = function(index){
		$window.open($scope.selectedCircler.facebook,'_system');
	};
        
        $scope.twitterClick = function(index){
		$window.open($scope.selectedCircler.twitter,'_system');
	};

		$scope.goSlider = function(){
		$state.go("slider");
		};
        
       
})

.controller('CircledetailMembersCtrl', function($scope, $state, $ionicScrollDelegate, ApiEndpoint, $stateParams, $ionicLoading, NTRService, Storage, AgendaService, LadiesService, $http) {
	$scope.activeUser = Storage.getObject("activeUser");
	console.log($scope.activeUser);
    $scope.selectedCircler = angular.fromJson($stateParams.selectedItem);
    $scope.activeUser.tbl=$scope.selectedCircler.circle;
	$scope.oldCirclersList = new Array();
	$scope.Circlers = new Array();
	$scope.current = { tab:"leden"};

	  $scope.changeTab = function(tabname){
		  $ionicScrollDelegate.resize();
		  $scope.current.tab = tabname;


	  }


	  $scope.goAllMyCirclerDetail = function(index){
        $state.go("app.Circler_detail", {"selectedItem":angular.toJson($scope.Circlers[index])});
	  };

	  	$scope.goSlider = function(){
		$state.go("slider");
		};

	  $scope.goOldCirclerDetail = function(index){
		$state.go("app.Circler_detail", {"selectedItem":angular.toJson($scope.oldCirclersList[index])});
	  };

	var getOldCirclers = function(){
        $ionicLoading.show();

		var promise = LadiesService.getOldMembersOfCircle1($scope.activeUser);
  		promise.then(function(result){

			if(result.data){
                            if(result.requests_left == "1")
                            {
                                ionicToast.show("U heeft voor vandaag helaas het maximum aantal LAPP requests verbruikt in het ledenadministratiesysteem van Ladies Circle Nederland.", 'bottom', false, 4000);
                            }
                            console.log(JSON.stringify(result.data));

				for(var index in result.data){
                                    if(result.data[index].member_type == 'Oud-lid' || result.data[index].member_type == 'VUT')
                                    {

                                           if(result.data[index].photo_last_modification_date != '')
                                        {
					result.data[index].photo = ApiEndpoint.url+"/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[index].member_id;
					   }else{
                                         result.data[index].photo = 'no image';
                                        }

                                        $scope.oldCirclersList.push(result.data[index]);
                                    }else{


                                           if(result.data[index].photo_last_modification_date != '')
                                        {

                            result.data[index].photo = ApiEndpoint.url+"/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[index].member_id;
					   }else{
                                         result.data[index].photo = 'no image';
                                        }
                                     	//alert(JSON.stringify(result.data[index].photo));


                                    $scope.Circlers.push(result.data[index]);
                                    }

                        }

			}
			console.log($scope.oldCirclersList);
			//def.resolve(1);

                       $ionicLoading.hide();

  		}, function(reason){
			console.log(reason);
			//def.resolve(0);
                        $ionicLoading.hide();
  		});

		//rootDefs.push(def.promise());
	};

	var getMembersOfCircles = function(){
		$ionicLoading.show();

		var promise = LadiesService.getMembersOfCircle1($scope.activeUser);
  		promise.then(function(result){

			if(result.data){

				for(var index in result.data){
                                          if(result.data[index].photo_last_modification_date != '')
                                        {

                            result.data[index].photo = ApiEndpoint.url+"/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[index].member_id;
					   }else{
                                         result.data[index].photo = 'no image';
                                        }
                                     	//alert(JSON.stringify(result.data[index].photo));


                                    $scope.Circlers.push(result.data[index]);




				}




                        ///        alert ($scope.Circlers);

			}
			console.log($scope.Circlers);
			$ionicLoading.hide();
  		}, function(reason){
			console.log(reason);
			$ionicLoading.hide();
  		});
               // rootDefs.push(def.promise());
        };

         getOldCirclers();

})

.controller('CirclerDetailCtrl', function($scope, $state, $cordovaCalendar, $cordovaSocialSharing,ionicToast, $stateParams, $ionicViewSwitcher, $ionicPopover,$ionicPopup, $cordovaContacts, $ionicPlatform, $q, UserService) {
	$scope.selectedCircler = angular.fromJson($stateParams.selectedItem);
  //    alert (JSON.stringify($scope.selectedCircler));
  
  /*
   
                          if(result.requests_left == "1")
                            {
                                ionicToast.show("U heeft voor vandaag helaas het maximum aantal LAPP requests verbruikt in het ledenadministratiesysteem van Ladies Circle Nederland.", 'bottom', false, 4000);
                            }
   
   */
        if($scope.selectedCircler.photo_last_modification_date == '')
                        {


                           $scope.selectedCircler.photo = 'no image';
                        }

	moment.locale('tq');
	$scope.selectedCircler.birthday = moment($scope.selectedCircler.bdate + "").lang('nl').format("DD-MM-YYYY");
	// alert($scope.selectedCircler.birthday);

         var ag = $scope.selectedCircler.birthday.split('-');

         var d= new Date();
         var birthDay = ag[0];
         var birthMonth=ag[1];
         var birthYear=ag[2];

         var currYear = d.getFullYear();
         var currMonth = d.getMonth();
         var currDay = d.getDate();

          var age = currYear - birthYear;
          if (currMonth < birthMonth - 1) {
                        age--;
          }
          if (birthMonth - 1 == currMonth && currDay < birthDay) {
                        age--;
           }

    ///     alert (age);

        $scope.selectedCircler.age =  age +' jaar';

            //moment($scope.selectedCircler.bdate + "").lang('nl').fromNow(true) ;
               // alert($scope.selectedCircler.age);

               // alert($scope.selectedCircler.age);

    console.log($scope.selectedCircler);

        // Ypsilon //

           $scope.emailsendto = $scope.selectedCircler.email;

           $scope.contactperson = $scope.selectedCircler.fullname;

           $scope.birthday = $scope.selectedCircler.birthday;


           $scope.cells = $scope.selectedCircler.cell;

           var values = $scope.cells.split("-");

           $scope.cell = values[0]+values[1];



        //Ypsilon //

	  var template = '<ion-popover-view style="height:150px;"><ion-content class="padding"><h5>Local Hero</h5>Badge voor de ladie die binnen zijn eigen Circle het meest gebruik maakt van LAPP.</ion-content></ion-popover-view>';

	  $scope.popover = $ionicPopover.fromTemplate(template, {
		scope: $scope
	  });

	  $scope.openPopover = function($event) {
		$scope.popover.show($event);
	  };
	  $scope.closePopover = function() {
		$scope.popover.hide();
	  };
	  //Cleanup the popover when we're done with it!
	  $scope.$on('$destroy', function() {
		$scope.popover.remove();
	  });
	  // Execute action on hide popover
	  $scope.$on('popover.hidden', function() {
		// Execute action
	  });
	  // Execute action on remove popover
	  $scope.$on('popover.removed', function() {
		// Execute action
	  });

	  $scope.goCirclerDetailEdit = function(){
			$state.go("app.Circler_detail_edit");
	  };

	  	$scope.goSlider = function(){
		$state.go("slider");
		};

	  $scope.goCirclersOnMap = function(){
			$state.go("app.Circlers_on_map", {"selectedItem":angular.toJson($scope.selectedCircler)});
	  };

	  $scope.onEmail = function(){
		$cordovaSocialSharing.shareViaEmail("", "Bericht vanuit Lapp", $scope.emailsendto, "", "", null).then(function(result) {
		  // Success!
		}, function(err) {
		  // An error occurred. Show a message to the user
		});

	  };

          $scope.addContact = function()
          {

              //var phoneNumbers = [];
   // phoneNumbers[0] = new ContactField('phoneNumbers', employee.get('mobilePhone'));
   // phoneNumbers[1] = new ContactField('phoneNumbers', employee.get('phone'));
    //$scope.contactForm.phoneNumbers = phoneNumbers;

        $scope.contactForm = {

    "displayName": $scope.contactperson,
    "name": {
        "givenName": $scope.contactperson,
        "formatted": $scope.contactperson
    },
    "nickname": null,
    "phoneNumbers": [
        {
            "value": $scope.cell,
            "type": "mobile"
        }
    ],
    "emails": [
        {
            "value": $scope.selectedCircler.email,
            "type": "home"
        }
    ],
    "addresses": [
        {
            "type": "home",
            "formatted": null,
            "streetAddress": $scope.selectedCircler.addr_street
        }
    ],
    "ims": null,
    "organizations": $scope.selectedCircler.Circle,
    "birthday": $scope.selectedCircler.birthday,
    "note": "",
    "photos": null,
    "categories": null,
    "urls": null
}

            $cordovaContacts.save($scope.contactForm).then(function(result) {
      // Contact saved
      ionicToast.show("Het contact is met succes toegevoegd aan uw contacten", 'bottom', false, 4000);

            }, function(err) {
      // Contact error
      ionicToast.show("Het toevoegen is helaas mislukt", 'bottom', false, 4000);

            });
          };

          $scope.CallNumber = function(){
                var number = $scope.cell ;
                
                 var confirmPopup = $ionicPopup.confirm({
     title: 'Bellen',
     template: 'Weet je zeker dat je wilt bellen?'
   });

   confirmPopup.then(function(res) {
     if(res) {
        window.plugins.CallNumber.callNumber(function(){
                 //success logic goes here

                }, function(){
                 //error logic goes here
                }, number)
     } else {
       console.log('Deletion canceled !');
     }
   });
                
               
         };

	  try {
				$cordovaCalendar.createCalendar({
					calendarName: 'Lapp',
					calendarColor: '#FF0000'
				}).then(function (result) {
					// success
				}, function (err) {
					// error
				});
		} catch (e) { }

		$scope.onCalander = function(){
				/*$cordovaCalendar.createEventInNamedCalendar({
					title: $scope.selectedCircler.type,
					location: "Lapp",
					notes: $scope.selectedAgenda.description,
					startDate: $scope.selectedAgenda.startdatetime,
					endDate: $scope.selectedAgenda.enddatetime,
					calendarName: 'Lapp'
				}).then(function (result) {
					// success
					ionicToast.show("Dit event is succesvol toegevoegd aan uw telefoon!", 'bottom', false, 3000);
					//$ionicHistory.goBack();
				}, function (err) {
					// error
				});*/
		};


})

.controller('CirclersOnMapCtrl', function($scope, $state, $stateParams, $cordovaContacts,$cordovaSocialSharing,$ionicLoading,$ionicPopup, GeoService, ionicToast, $log, $timeout, $cordovaGeolocation, $rootScope) {
	$scope.selectedCircler = angular.fromJson($stateParams.selectedItem);
	console.log($scope.selectedCircler);

         if($scope.selectedCircler.photo_last_modification_date == '')
                        {


                           $scope.selectedCircler.photo = 'no image';
                        }

       // alert(JSON.stringify($scope.selectedCircler));
	var getLocation = function(){
		$ionicLoading.show();
		var promise = GeoService.Geocoding($scope.selectedCircler.addr_postcalcode);
  		promise.then(function(result){
			console.log(result.results[0].geometry.location);
                        
			$scope.map = {center: {latitude: result.results[0].geometry.location.lat, longitude: result.results[0].geometry.location.lng }, zoom: 14 }
			$scope.options = {
				  draggable: true,
				  zoomControl: false,
				  scrollwheel: false,
				  disableDoubleClickZoom: true,
				  disableDefaultUI: true
			};
			$scope.marker = {
				coords: {
					latitude: result.results[0].geometry.location.lat,
					longitude: result.results[0].geometry.location.lng
				},
				show: false,
				id: 0
			};

			$ionicLoading.hide();
  		}, function(reason){
			console.log(reason);
			$ionicLoading.hide();
  		});
	};
	getLocation();

         $scope.goCirclerDetail = function(){
            //  alert("hello");
		  $state.go("app.Circler_detail", {"selectedItem":angular.toJson($scope.selectedCircler)});
	  };

	  	$scope.goSlider = function(){
		$state.go("slider");
		};


    $scope.windowOptions = {
            visible: true
    };

    $scope.onClick = function() {
      //  alert('visible');
            $scope.windowOptions.visible = !$scope.windowOptions.visible;
    };

    $scope.closeClick = function() {
       // alert('close');
        $scope.windowOptions.visible = true;
    };

          $scope.onEmail = function(){
		$cordovaSocialSharing.shareViaEmail("", "Bericht vanuit Lapp", $scope.selectedCircler.email , "", "", null).then(function(result) {
		  // Success!
		}, function(err) {
		  // An error occurred. Show a message to the user
		});

	  };

          $scope.addContact2 = function()
          {

        $scope.contactForm = {

    "displayName": $scope.selectedCircler.fullname,
    "name": {
        "givenName": $scope.selectedCircler.fullname,
        "formatted": $scope.selectedCircler.fullname
    },
    "nickname": null,
    "phoneNumbers": [
        {
            "value": $scope.selectedCircler.cell,
            "type": "mobile"
        }
    ],
    "emails": [
        {
            "value": $scope.selectedCircler.email,
            "type": "home"
        }
    ],
    "addresses": [
        {
            "type": "home",
            "formatted": null,
            "streetAddress": $scope.selectedCircler.addr_street
        }
    ],
    "ims": null,
    "organizations": $scope.selectedCircler.Circle,
    "birthday": null,
    "note": "",
    "photos": null,
    "categories": null,
    "urls": null
}

            $cordovaContacts.save($scope.contactForm).then(function(result) {
      // Contact saved
            ionicToast.show("Het contact is met succes toegevoegd aan uw contacten", 'bottom', false, 4000);

            }, function(err) {
      // Contact error
            ionicToast.show("Het toevoegen is helaas mislukt", 'bottom', false, 4000);

            });
          };

          $scope.CallNumber = function(){
                var number = $scope.selectedCircler.cell ;
                
                
                 var confirmPopup = $ionicPopup.confirm({
     title: 'Bellen',
     template: 'Weet je zeker dat je wilt bellen?'
   });

   confirmPopup.then(function(res) {
     if(res) {
         
                window.plugins.CallNumber.callNumber(function(){
                 //success logic goes here
                }, function(){
                 //error logic goes here
                }, number)
       console.log('Deleted !');
     } else {
       console.log('Deletion canceled !');
     }
   });
                
                
         };



})

.controller('CirclerDetailEditCtrl', function($scope, $cordovaFileTransfer, $ionicPopup,$state, $cordovaImagePicker, LadiesService,$cordovaCamera, ionicToast, $ionicLoading, $stateParams, Storage,$http) {
	$scope.activeUser = Storage.getObject("activeUser");
	$scope.selectedCircler = angular.fromJson($stateParams.selectedItem);
	console.log($scope.selectedCircler);
      
        if($scope.selectedCircler.photo_last_modification_date == '')
                        {


                           $scope.selectedCircler.photo = 'no image';
                        }
    $scope.selectedCircler.last_seen = moment($scope.selectedCircler.last_seen + "").lang('nl').format("DD-MM-YYYY HH:MM");

	$scope.setting = {
		push:false,
		geo:false,
		facebook:false,
		photo:false
	}

	if(Storage.getObject("setting")){
		$scope.setting = Storage.getObject("setting");
	}

		$scope.onSave = function(selectedCircler){
		$ionicLoading.show();
		Storage.setObject("setting", $scope.setting);


		var userObject = {
                occupation:selectedCircler.occupation,
                addr_street: selectedCircler.addr_street,
                addr_postalcode: selectedCircler.addr_postcalcode,
                addr_city: selectedCircler.addr_city,
                cell: selectedCircler.cell,
                spouse: selectedCircler.spouse,
                kids: selectedCircler.kids,
       };

	   var _param ={
			email:$scope.activeUser.email,
			password:$scope.activeUser.password,
			data:userObject
	   }

		console.log(_param);

		var promise = LadiesService.updateMyProfile(_param);
  		promise.then(function(result){
                    
                          if(result.requests_left == "1")
                            {
                                ionicToast.show("U heeft voor vandaag helaas het maximum aantal LAPP requests verbruikt in het ledenadministratiesysteem van Ladies Circle Nederland.", 'bottom', false, 4000);
                            } 
                   
			$ionicLoading.hide();
			if(result.result == "nok"){
				ionicToast.show(result.reason, 'bottom', false, 3000);
			}else{
				ionicToast.show("Goedzo! Je profiel is met succes aangepast", 'bottom', false, 3000);

			}
  		}, function(reason){
			console.log(reason);
			$ionicLoading.hide();
  		});
	};

  $scope.takePicture = function()
    {
        
        var myPopup = $ionicPopup.show({
    template: '',
    title: 'Gebruik foto van telefoon',
    buttons: [
      { text: 'Camera',
       type: 'button-positive',
        onTap: function(e) {
          
                                fromCamera();
        }},
      {
        text: 'Gallery',
        type: 'button-positive',
        onTap: function(e) {
                                fromGallery();
        }
      }
    ]
  });
        
        

       var fromCamera = function () {
                var options = {
                    quality: 60,
                    destinationType: Camera.DestinationType.DATA_URL,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: true,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 800,
                    targetHeight: 800,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: true
                };

                $cordovaCamera.getPicture(options).then(function (imageData) {
                    $scope.imgURI = "data:image/jpeg;base64," + imageData;
                    uploadPic();
                }, function (err) {
                    // An error occured. Show a message to the user
                });
            };
       
            
             var uploadPic = function () {
                var options = {
                    fileKey: "data",
                    fileName: 'image.png',
                    chunkedMode: false,
                    mimeType: 'image/png'
                };
                $cordovaFileTransfer.upload("https://lcnl.interleave.nl/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=SetMemberImage&member="+$scope.selectedCircler.member_id, $scope.imgURI, options).then(function (result) {
                   // alert("");
                     ionicToast.show("Je foto is successvol geupload!", 'bottom', false, 4000);
                   // alert("SUCCESS" + JSON.stringify(result));
                }, function (err) {
                   // alert('Image Upload Error');
                    ionicToast.show("Image Upload Error", 'bottom', false, 4000);
//                    console.log("ERROR: " + JSON.stringify(err));
                }, function (progress) {
                    // constant progress updates
                });
            };
            
            

            var fromGallery = function(){
                
            navigator.camera.getPicture(onSuccess, onFail, { quality: 75, targetWidth: 800, targetHeight: 800, 
            destinationType: Camera.DestinationType.DATA_URL,sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY
         });

              function onSuccess(imageData) {
                   $scope.imgURI = "data:image/jpeg;base64," + imageData;
                    uploadPic();
                //  alert('Success from gallery');
            }
            function onFail(message) {
                alert('Failed because: ' + message);
            }
            };


};

                $scope.takefacebookPicture = function(){

                   // alert("hello");

                    $scope.activeUser = Storage.getObject("activeUser");

                         // This is the success callback from the login method
  var fbLoginSuccess = function(response) {
    if (!response.authResponse){
      fbLoginError("Cannot find the authResponse");
      return;
    }

    var authResponse = response.authResponse;

    getFacebookProfileInfo(authResponse)
    .then(function(profileInfo) {
      // For the purpose of this example I will store user data on local storage
      UserService.setUser({
        authResponse: authResponse,
				userID: profileInfo.id,
				name: profileInfo.name,
				email: profileInfo.email,
        picture : "http://graph.facebook.com/" + authResponse.userID + "/picture?type=large"
      });
      $ionicLoading.hide();
      //$state.go('app.home');
    }, function(fail){
      // Fail get profile info
      console.log('profile info fail', fail);
    });
  };

  var fbLoginError = function(error){
    console.log('fbLoginError', error);
    $ionicLoading.hide();
  };

 
  var getFacebookProfileInfo = function (authResponse) {
    var info = $q.defer();

    facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null,
      function (response) {
				console.log(response);
        info.resolve(response);
      },
      function (response) {
				console.log(response);
        info.reject(response);
      }
    );
    return info.promise;
  };

 
  var facebookSignIn = function() {
    facebookConnectPlugin.getLoginStatus(function(success){
      if(success.status === 'connected'){
       
        console.log('getLoginStatus', success.status);

    		// Check if we have our user saved //
                
    		var user = UserService.getUser('facebook');

                console.log(JSON.stringify(user));

    		if(!user.userID){
					getFacebookProfileInfo(success.authResponse)
					.then(function(profileInfo) {
						// For the purpose of this example I will store user data on local storage
						UserService.setUser({
							authResponse: success.authResponse,
							userID: profileInfo.id,
							name: profileInfo.name,
							email: profileInfo.email,
							picture : "http://graph.facebook.com/" + success.authResponse.userID + "/picture?type=large"
						});


                            // Facebook Image Upload From Url //


                            var server ="https://lcnl.interleave.nl/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=SetMemberImage&member="+$scope.selectedCircler.file;

                             // Download Image From Url //

                            document.addEventListener('deviceready', function () {

                                    var url = "http://graph.facebook.com/" + success.authResponse.userID + "/picture?type=large";
                                    var targetPath = cordova.file.documentsDirectory + "test.png";
                                    var trustHosts = true;
                                    var options = {};

                                    $cordovaFileTransfer.download(url, targetPath, options, trustHosts)
                                      .then(function(result) {

                                        console.log("Image Downloaded In Gallery");

                                      }, function(err) {
                                        // Error
                                      }, function (progress) {
                                        $timeout(function () {
                                          $scope.downloadProgress = (progress.loaded / progress.total) * 100;
                                        });
                                      });

                                   }, false);
                                   
                            // Image Downloaded From Url //
                            
                            
                            // Upload Downloaded Image On Live Server //


                                  document.addEventListener('deviceready', function () {

                                    $cordovaFileTransfer.upload(server, filePath, options)
                                      .then(function(result) {

                                          console.log("file Uploaded Successfully");

                                        // Success!
                                      }, function(err) {
                                        // Error
                                      }, function (progress) {
                                        // constant progress updates
                                      });

                               }, false);


                            // File Upload From Url //


					//	$state.go('app.home');
					}, function(fail){
						// Fail get profile info
						console.log('profile info fail', fail);
					});
				}else{
					//$state.go('app.home');
				}
      } else {
				console.log('getLoginStatus', success.status);

				$ionicLoading.show({
          template: 'Logging in...'
        });
			
        facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
      }
    });
  };

  facebookSignIn();

		};


})



.controller('AgendaDetailCtrl', function($scope, $state, $stateParams, $cordovaSocialSharing, $cordovaCalendar, $cordovaInAppBrowser, ionicToast) {
    $scope.selectedAgenda = angular.fromJson($stateParams.selectedItem);
	$scope.selectedAgenda.time = moment($scope.selectedAgenda.startdatetime + "").lang('nl').format("HH:mm");
        
        if($scope.selectedAgenda.url)
        {
            
        }else{
            $scope.selectedAgenda.url = '';
        }
        
	console.log($scope.selectedAgenda);

      //  alert (JSON.stringify($scope.selectedAgenda));

	var defaultOptions = {
		"location":"yes",
		"clearcache":"no",
		"toolbar":"yes"
	};

	$scope.openBrowser = function(){
		$cordovaInAppBrowser.open($scope.selectedAgenda.url, '_blank', defaultOptions).then(function(event){
		}).catch(function(event){
		});
	};

	try {
            $cordovaCalendar.createCalendar({
                calendarName: 'Lapp',
                calendarColor: '#FF0000'
            }).then(function (result) {
                // success
            }, function (err) {
                // error
            });
    } catch (e) { }

	$scope.onCal = function(){
			$cordovaCalendar.createEventInNamedCalendar({
            title: $scope.selectedAgenda.description,
            location: $scope.selectedAgenda.location,
            notes: $scope.selectedAgenda.type,
            startDate: $scope.selectedAgenda.startdatetime,
            endDate: $scope.selectedAgenda.enddatetime,
            calendarName: 'Lapp'
            }).then(function (result) {
                // success
                ionicToast.show("Activiteit is succesvol toegevoegd.", 'bottom', false, 3000);
                //$ionicHistory.goBack();
            }, function (err) {
                // error
            });
	};


        // var share= 'Tafel '+$scope.selectedAgenda.Circle+' '+$scope.selectedAgenda.Circlename+' '+$scope.selectedAgenda.description;
        var share= 'Ben jij er ook bij? '+$scope.selectedAgenda.description+' LC '+$scope.selectedAgenda.circle+' '+$scope.selectedAgenda.circlename+' op '+$scope.selectedAgenda.date+' '+$scope.selectedAgenda.month+' '+$scope.selectedAgenda.year+' '+$scope.selectedAgenda.url;
       // alert(share);

	$scope.onTwitter = function(){
		$cordovaSocialSharing.shareViaTwitter(share, null, '').then(function(result) {
			  // Success!
			}, function(err) {
			  // An error occurred. Show a message to the user
			});
	};
        
        var sharviaurl = $scope.selectedAgenda.url;

	$scope.onFacebook = function(){
		$cordovaSocialSharing.shareViaFacebook(null, null, sharviaurl).then(function(result) {
			  // Success!
			}, function(err) {
			  // An error occurred. Show a message to the user
			});
	};

	$scope.onLinkdin = function(){
		 $cordovaSocialSharing.shareViaLinkedIn(share, "Lapp", null, '').then(function(result) {
                  // Success!
         }, function(err) {
                  // An error occured. Show a message to the user
         });
	};

	$scope.onEmail = function(){
		$cordovaSocialSharing.shareViaEmail(share, "Bericht vanuit Lapp", "$scope.emailsendto", "", "", null).then(function(result) {
		  // Success!
		}, function(err) {
		  // An error occurred. Show a message to the user
		});

	};

		$scope.goSlider = function(){
		$state.go("slider");
		};
})

.controller('NewsDetailCtrl', function($scope, $state, $stateParams,$cordovaSocialSharing,$window) {
	$scope.AdvertiserOneWebsiteClick = function(index){
		$window.open('https://www.lappadmin.nl/advertiser-one-websiteclick.html','_system');
	};
	$scope.AdvertiserTwoWebsiteClick = function(index){
		$window.open('https://www.lappadmin.nl/advertiser-two-websiteclick.html','_system');
	};	
	$scope.selectedNews = angular.fromJson($stateParams.selectedItem);
	console.log($scope.selectedNews);
	$scope.onCal = function(){};

	$scope.onIAB = function(){};

        var share= $scope.selectedNews.title;
        var shareWithContent= $scope.selectedNews.title+" "+$scope.selectedNews.content;
	$scope.onTwitter = function(){
		$cordovaSocialSharing.shareViaTwitter(share, null, '').then(function(result) {
			  // Success!
			}, function(err) {
			  // An error occurred. Show a message to the user
			});
	};
        
        var sharwithurl = $scope.selectedNews.url;
        
        //alert(sharwithurl);

	$scope.onFacebook = function(){
		$cordovaSocialSharing.shareViaFacebook(null, null, sharwithurl).then(function(result) {
			  // Success!
			}, function(err) {
                            alert(err);
			  // An error occurred. Show a message to the user
			});
	};

	$scope.onLinkdin = function(){
		 $cordovaSocialSharing.share(shareWithContent, "Lapp", null, '').then(function(result) {
                  // Success!
         }, function(err) {
                  // An error occured. Show a message to the user
         });
	};

	$scope.onEmail = function(){
		$cordovaSocialSharing.shareViaEmail(shareWithContent, "Bericht vanuit Lapp", "$scope.emailsendto", "", "", null).then(function(result) {
		  // Success!
		}, function(err) {
		  // An error occurred. Show a message to the user
		});

	};

		$scope.goSlider = function(){
		$state.go("slider");
		};
})

.controller('CircleCtrl', function($scope, $state, $window, $ionicScrollDelegate, ApiEndpoint, $ionicLoading, NTRService, Storage, AgendaService, LadiesService, $http) {
	  $scope.activeUser = Storage.getObject("activeUser");
	  console.log($scope.activeUser);

	  $scope.graph = {};
	  $scope.graph.data = [
		//Awake
		[16, 45, 20, 32, 16, 30, 23],

	  ];
	  $scope.graph.labels = ['2010', '2011', '2012', '2013', '2014', '2015', '2016'];
	  $scope.graph.series = ['Awake'];

	  $scope.current = { tab:"dashboard"};
	  $scope.changeTab = function(tabname){
		  $ionicScrollDelegate.resize();
		  $scope.current.tab = tabname;
	  }

	  $scope.goAgendaDetail = function(index){
		$state.go("app.agenda_detail", {"selectedItem":angular.toJson($scope.agendaList[index])});
	  };

	  $scope.goCirclerDetail = function(index){
		  $state.go("app.Circler_detail", {"selectedItem":angular.toJson($scope.myCirclers[index])});
	  };

	  $scope.goAllMyCirclerDetail = function(index){
		  $state.go("app.Circler_detail", {"selectedItem":angular.toJson($scope.allMyCirclers[index])});
	  };

	  $scope.goOldCirclerDetail = function(index){
		  $state.go("app.Circler_detail", {"selectedItem":angular.toJson($scope.oldCirclersList[index])});
	  };

	  $scope.goAgenda = function(){
		$state.go("app.agenda");
	  };

	  $scope.sCircleDetail = function(){
		  $state.go("app.Circle_detail", {"selectedItem":angular.toJson($scope.myCircle)});
	  };

	  	$scope.goSlider = function(){
		$state.go("slider");
		};

	$scope.MyCircleId = 27;

	var rootDefs = [];
	$scope.myCirclers = new Array();
	$scope.allMyCirclers = new Array();
	$scope.agendaList = new Array();
	$scope.ntrList = new Array();
	$scope.myCircle = "";
        $scope.myroleinCircle1 = "";
        $scope.myroleinCircle2 = "";
        $scope.myroleinCircle3 = "";
        $scope.myroleinCircle4 = "";
        $scope.myroleinCircle5 = "";
		$scope.oldCirclersList = new Array();

	var getOldCirclers = function(){
		var def = $.Deferred();

		var promise = LadiesService.getOldMembersOfCircle($scope.activeUser);
  		promise.then(function(result){


			if(result.data){
                            
                            if(result.requests_left == "1")
                            {
                                ionicToast.show("U heeft voor vandaag helaas het maximum aantal LAPP requests verbruikt in het ledenadministratiesysteem van Ladies Circle Nederland.", 'bottom', false, 4000);
                            }
				for(var index in result.data){
                                    if(result.data[index].member_type == 'Oud-lid')
                                    {

                                           if(result.data[index].photo_last_modification_date != '')
                                        {
					result.data[index].photo = ApiEndpoint.url+"/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[index].member_id;
					   }else{
                                         result.data[index].photo = 'no image';
                                        }

                                        $scope.oldCirclersList.push(result.data[index]);
                                    }
				}
			}
			console.log($scope.oldCirclersList);
			def.resolve(1);
  		}, function(reason){
			console.log(reason);
			def.resolve(0);
  		});

		rootDefs.push(def.promise());
	};

	var getAgenda = function(){
		var def = $.Deferred();

		var promise = AgendaService.myCircleAgenda($scope.activeUser);
  		promise.then(function(result){

			if(result.data){
				for(var index in result.data){
					if(index < 5)
					{
						result.data[index].date = moment(result.data[index].startdatetime + "").lang('nl').format("D");
						result.data[index].month = moment(result.data[index].startdatetime + "").lang('nl').format("MMMM");
						result.data[index].year = moment(result.data[index].startdatetime + "").lang('nl').format("YYYY");
						$scope.agendaList.push(result.data[index]);
					}
				}
			}
			console.log($scope.agendaList);
			def.resolve(1);
  		}, function(reason){
			console.log(reason);
			def.resolve(0);
  		});

		rootDefs.push(def.promise());
	};

	var getNTR = function(){
		var def = $.Deferred();

		var promise = NTRService.myCircleNTR($scope.activeUser);
  		promise.then(function(result){

			if(result.data){
				for(var index in result.data){
					if(index < 5)
					{
						$scope.ntrList.push(result.data[index]);
					}
				}
			}
			console.log($scope.ntrList);

			  $scope.graph = {};
			  $scope.graph.data = [
					[$scope.ntrList[0]["#circlers3jaargeleden"],
					$scope.ntrList[0]["#circlers2jaargeleden"],
					$scope.ntrList[0]["#circlers1jaargeleden"],
					$scope.ntrList[0]["#circlers"],
					$scope.ntrList[0]["#circlers1jaarindetoekomst"]],

			  ];
			  $scope.graph.labels = ['2013', '2014', '2015', '2016', '2017'];
			  $scope.graph.series = ['Awake'];

			def.resolve(1);
  		}, function(reason){
			console.log(reason);
			def.resolve(0);
  		});

		rootDefs.push(def.promise());
	};

	var getCircleData = function(){
		var def = $.Deferred();
		var promise = LadiesService.myCircle($scope.activeUser);

          	promise.then(function(result){
			if(result.data){
				for(var index1 in result.data){
					if(result.data[index1].circle == $scope.MyCircleId)
					{
						$scope.myCircle = result.data[index1];
						$scope.myCircle.date = moment(result.data[index1].charter_date + "").lang('nl').format("D-MM-YYYY");
                                        }
				}
			}
			console.log($scope.myCircle);
			def.resolve(1);
  		}, function(reason){
			console.log(reason);
			def.resolve(0);
  		});

		rootDefs.push(def.promise());
	};

	var getMyCircleDashboardData = function(){
		$ionicLoading.show();

		var promise = LadiesService.getMembersOfCircle($scope.activeUser);
  		promise.then(function(result){

			if(result.data){
                          //alert(JSON.stringify(result.data));
				for(var index in result.data){

                                  // alert(index);
                                   if(result.data[index].photo_last_modification_date != '')
                        {
                                          result.data[index].photo = ApiEndpoint.url+"/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[index].member_id;
                                       }else{
                            result.data[index].photo = 'no image';
                        }
					if(index < 4)
					{
						$scope.myCirclers.push(result.data[index]);
					}

					$scope.allMyCirclers.push(result.data[index]);
					$scope.MyCircleId = result.data[0].circle;
                                       // alert($scope.MyCircleId);

                                                       if ( result.data[index].role_in_circle == 'Voorzitter')
                                                    {

                                                     $scope.myroleinCircle1 = result.data[index];
                                                     $scope.myroleinCircle1.ind=index;

                                                    if(result.data[index].photo_last_modification_date != '')
                                                     {
                                                                $scope.myroleinCircle1.photo = ApiEndpoint.url+"/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[index].member_id;
                                                     }else{
                                                                   $scope.myroleinCircle1.photo = 'no image';
                                                  }
                                                 }
                                                  if ( result.data[index].role_in_circle == 'Secretaris')
                                                    {

                                                     $scope.myroleinCircle2 = result.data[index];
                                                     $scope.myroleinCircle2.ind1=index;
                                                     if(result.data[index].photo_last_modification_date != '')
                                                     {
                                                                $scope.myroleinCircle2.photo = ApiEndpoint.url+"/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[index].member_id;
                                                     }else{
                                                                   $scope.myroleinCircle2.photo = 'no image';
                                                  }
                                                 }
                                                 if ( result.data[index].role_in_circle == 'Penningmeester')
                                                    {

                                                     $scope.myroleinCircle3 = result.data[index];
                                                        $scope.myroleinCircle3.ind2=index;
                                                     if(result.data[index].photo_last_modification_date != '')
                                                     {
                                                                $scope.myroleinCircle3.photo = ApiEndpoint.url+"/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[index].member_id;
                                                     }else{
                                                                   $scope.myroleinCircle3.photo = 'no image';
                                                  }
                                                 }
                                                 if ( result.data[index].role_in_circle == 'IRO')
                                                    {

                                                     $scope.myroleinCircle4 = result.data[index];
                                                     $scope.myroleinCircle4.ind3=index;
                                                     if(result.data[index].photo_last_modification_date != '')
                                                     {
                                                                $scope.myroleinCircle4.photo = ApiEndpoint.url+"/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[index].member_id;
                                                     }else{
                                                                   $scope.myroleinCircle4.photo = 'no image';
                                                  }
                                                 }
                                                 if ( result.data[index].role_in_circle == 'PRO')
                                                    {

                                                     $scope.myroleinCircle5 = result.data[index];
                                                     $scope.myroleinCircle5.ind4=index;
                                                     if(result.data[index].photo_last_modification_date != '')
                                                     {
                                                                $scope.myroleinCircle5.photo = ApiEndpoint.url+"/modules.php?action=run&mid=68&username="+$scope.activeUser.email+"&password="+$scope.activeUser.password+"&deviceid=9668702117633076&method=GetMemberImageByIdMedium&data="+result.data[index].member_id;
                                                     }else{
                                                                   $scope.myroleinCircle5.photo = 'no image';
                                                  }
                                                 }

                        }
			}
			console.log($scope.allMyCirclers);

			rootDefs = [];
			getAgenda();
			getNTR();
			getCircleData();
			getOldCirclers();

			$.when.apply($, rootDefs).then(function() {
				console.log("Als je dit leest zijn alle rollen volgens plan verwerkt!");
				$ionicLoading.hide();
			});

  		}, function(reason){
			console.log(reason);
			$ionicLoading.hide();
  		});


	};
	getMyCircleDashboardData();
        
        $scope.websiteClick = function(index){
		$window.open($scope.myCircle.website,'_system');
	};
        
        $scope.facebookClick = function(index){
		$window.open($scope.myCircle.facebook,'_system');
	};
        
        $scope.twitterClick = function(index){
		$window.open($scope.myCircle.twitter,'_system');
	};

})



.controller('NewsCtrl', function($scope, $state, $ionicLoading, Storage, NewsService, $window) {
	  $scope.activeUser = Storage.getObject("activeUser");
	  console.log($scope.activeUser);


	  $scope.goHome = function(){
	  $state.go("app.home");
		};

	  $scope.goNewsDetail = function(index){
		$state.go("app.news_detail", {"selectedItem":angular.toJson($scope.newsList[index])});
	  };

	  	$scope.goSlider = function(){
		$state.go("slider");
		};

  	  $scope.newsList = new Array();
	  var getNews = function(){
		$ionicLoading.show();

		var promise = NewsService.all($scope.activeUser);
  		promise.then(function(result){
			if(result.posts){
				for(var index in result.posts){
					if(index < 20)
                                        {
                                            $scope.newsList.push(result.posts[index]);
                                           
                                            $scope.newsList[index].content = $scope.newsList[index].content.replace(/<[^>]+>/gm, '');
                                            
                                        }
				}
			}
			console.log($scope.newsList);

			$ionicLoading.hide();
  		}, function(reason){
			console.log(reason);
			$ionicLoading.hide();
  		});
	};
	getNews();
        
        $scope.AdvertiserOneWebsiteClick = function(index){
		$window.open('https://www.lappadmin.nl/advertiser-one-websiteclick.html','_system');
	};
})



.controller('NTRCtrl', function($scope, $state, $ionicLoading, Storage, $ionicScrollDelegate, NTRService) {
	  $scope.activeUser = Storage.getObject("activeUser");
	  console.log($scope.activeUser);

	  $scope.goNTRDetail = function(index){
		$state.go("app.articles", {"selectedItem":angular.toJson($scope.NTRList[index])});
	  };

	  	$scope.goSlider = function(){
		$state.go("slider");
		};

	  $scope.NTRList = new Array();
	  var getNTR = function(){
		$ionicLoading.show();

		var promise = NTRService.allList($scope.activeUser);
  		promise.then(function(result){
			if(result.data){
                            if(result.requests_left == "1")
                            {
                                ionicToast.show("U heeft voor vandaag helaas het maximum aantal LAPP requests verbruikt in het ledenadministratiesysteem van Ladies Circle Nederland.", 'bottom', false, 4000);
                            }
				for(var index in result.data){
					if(angular.toJson(result.data[index]).indexOf("LCI") != -1){

					}else{
						$scope.NTRList.push(result.data[index]);
					}
				}
			}
			console.log($scope.NTRList);
			$ionicLoading.hide();
  		}, function(reason){
			console.log(reason);
			$ionicLoading.hide();
  		});
	 };
	 getNTR();
})

.controller('RTICtrl', function($scope, $state, $ionicLoading, Storage, $ionicScrollDelegate, NTRService) {
	  $scope.activeUser = Storage.getObject("activeUser");
	  console.log($scope.activeUser);

	  $scope.goNTRDetail = function(index){
		$state.go("app.articles", {"selectedItem":angular.toJson($scope.NTRList[index])});
	  };

	  $scope.NTRList = new Array();
	  var getNTR = function(){
		$ionicLoading.show();

		$scope.goSlider = function(){
		$state.go("slider");
		};

		var promise = NTRService.allList($scope.activeUser);
  		promise.then(function(result){
			if(result.data){
                            if(result.requests_left == "1")
                            {
                                ionicToast.show("U heeft voor vandaag helaas het maximum aantal LAPP requests verbruikt in het ledenadministratiesysteem van Ladies Circle Nederland.", 'bottom', false, 4000);
                            }
				for(var index in result.data){
					if(angular.toJson(result.data[index]).indexOf("LCI") != -1){
						$scope.NTRList.push(result.data[index]);
					}
				}
			}
			console.log($scope.NTRList);
			$ionicLoading.hide();
  		}, function(reason){
			console.log(reason);
			$ionicLoading.hide();
  		});
	 };
	 getNTR();
})


.controller('SecretCtrl', function($scope, $state, $http, SecretService, Storage, AgendaService, $stateParams, $ionicLoading, $ionicScrollDelegate, $cordovaSocialSharing, $cordovaCalendar, $cordovaInAppBrowser, ionicToast) {
$scope.activeUser = Storage.getObject("activeUser");

    $scope.onSave = function(selectedCircler){
    $scope.selectedCircler = selectedCircler;
	console.log($scope.selectedCircler);


	$ionicLoading.show();
       var _param = {
                        email:$scope.activeUser.email,
			password:$scope.activeUser.password,
			data:$scope.selectedCircler.about_me,
			member_id:$scope.myInformation.member_id,
	   }


		var promise = SecretService.AddSecret(_param);
  		promise.then(function(response){
			console.log(response);
			$ionicLoading.hide();
			if(response.result == 'ok'){
				ionicToast.show(response.reason, 'bottom', false, 3000);
			}else{
				ionicToast.show("Your over mij tekst is succesvol toegevoegd", 'bottom', false, 3000);
			}
  		}, function(reason){
			console.log(reason);
			$ionicLoading.hide();

  		});
	};

		$scope.goSlider = function(){
		$state.go("slider");
		};

})


// End code Jaap

.controller('ArticleCtrl', function($scope, $state, Storage, $stateParams, $ionicLoading, NTRService) {
	  $scope.activeUser = Storage.getObject("activeUser");
	  console.log($scope.activeUser);

	  $scope.selectedArticle = angular.fromJson($stateParams.selectedItem);
	  console.log($scope.selectedArticle);

	  	$scope.goSlider = function(){
		$state.go("slider");
		};

	  $scope.articleData = "";
	  var getArticleData = function(){
		$ionicLoading.show();
		var _param = {
			email:$scope.activeUser.email,
			password:$scope.activeUser.password,
			id:$scope.selectedArticle["page-LAS-id"]
		}
		var promise = NTRService.getStaticPage(_param);
  		promise.then(function(result){
			if(result.data){
				$scope.articleData = result.data;
			}
			console.log(result);
			$ionicLoading.hide();
  		}, function(reason){
			console.log(reason);
			$ionicLoading.hide();
  		});
	 };
	 getArticleData();
})



.controller('StatisticCtrl', function($scope, $state, Storage, ApiEndpoint, $ionicLoading, $ionicScrollDelegate, CircleService, AgendaService, NTRService, LadiesService, NewsService, ionicToast) {
	$scope.activeUser = Storage.getObject("activeUser");

		$scope.goSlider = function(){
		$state.go("slider");
		};

	console.log($scope.activeUser);

	  $scope.graph = {};
	  $scope.graph.data = [
		//Awake
		[16, 45, 20, 32, 16, 30, 23],

	  ];
	  $scope.graph.labels = ['2010', '2011', '2012', '2013', '2014', '2015', '2016'];
	  $scope.graph.series = ['Awake'];

	  $scope.current = { tab:"dashboard"};
	  $scope.changeTab = function(tabname){
		$ionicScrollDelegate.resize();
		$scope.current.tab = tabname;
	  }

	var rootDefs = [];
	$scope.newestCirclers = new Array();
	$scope.newsList = new Array();
	$scope.agendaList = new Array();
	$scope.ntrList = new Array();
	$scope.CircleList = new Array();
	$scope.districtList = new Array();

	

	var getNTR = function(){
		var def = $.Deferred();

		var promise = NTRService.all($scope.activeUser);
  		promise.then(function(result){

			if(result.data){
				for(var index in result.data){
					if(index < 4)
					{
						$scope.ntrList.push(result.data[index]);
					}
				}
			}
			console.log($scope.ntrList[0]);

			  $scope.graph = {};
			  $scope.graph.data = [
				//Awake
				[$scope.ntrList[0]["#circlers3jaargeleden"],
					$scope.ntrList[0]["#circlers2jaargeleden"],
					$scope.ntrList[0]["#circlers1jaargeleden"],
					$scope.ntrList[0]["#circlers"],
					$scope.ntrList[0]["#circlers1jaarindetoekomst"]],

			  ];
			  $scope.graph.labels = ['2013', '2014', '2015', '2016', 'Huidig'];
			  $scope.graph.series = ['Awake'];

			def.resolve(1);
  		}, function(reason){
			console.log(reason);
			def.resolve(0);
  		});

		rootDefs.push(def.promise());
	};
	
	var getStatsData = function(){
		$ionicLoading.show();

		rootDefs = [];
		getNTR();

		$.when.apply($, rootDefs).then(function() {
			console.log("all things done");
			$ionicLoading.hide();
		});
	};
	getStatsData();

})


;
