// Copyright Jaap Harmsma & Rob Hopster 2016-2017 **Just Add Sugar B.V.**
// info@justaddsugar.nl of http://www.justaddsugar.nl

angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'chart.js', 'ionic-toast', 'ngCordova', 'uiGmapgoogle-maps', 'ionic-cache-src','LocalForageModule'])


.run(function($ionicPlatform, $state, $cordovaSQLite) {
  $ionicPlatform.ready(function() {

    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
     cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
     cordova.plugins.Keyboard.disableScroll(true);

    }

    if (window.StatusBar) {
      StatusBar.styleDefault();
    }


	if(navigator.network && navigator.network.connection.type == "none"){
		ionicToast.show("You don't appear to have an active connection. Please check your network status.", 'bottom',false, 10000);
		$state.go("activate_request");
	}

	var onOffline = function(){
		ionicToast.show("You don't appear to have an active connection. Please check your network status.", 'bottom',false, 10000);
		$state.go("activate_request");
	};

	document.addEventListener("offline", onOffline, false);

    if(window.screen)
		screen.lockOrientation('portrait');
        
  });
  
})

.config(
    ['uiGmapGoogleMapApiProvider', function(GoogleMapApiProviders) {
        GoogleMapApiProviders.configure({
            china: true
        });
    }],
['$localForageProvider', function($localForageProvider) {
	$localForageProvider.config({
		name: 'LAPP' // name of the database and prefix for your data
	});
}]
)

.constant('ApiEndpoint', {
  url: 'https://lcnl.interleave.nl/'
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $ionicConfigProvider.views.swipeBackEnabled(true);
  $ionicConfigProvider.views.maxCache(5);
  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider


  // setup an abstract state for the tabs directive

   .state('login', {
        url: '/login',
        controller: 'LoginCtrl',
        templateUrl: 'templates/login.html'
    })

   /*
  .state('activate', {
    url: '/activate',
    abstract: true,
    templateUrl: 'templates/activate.html'
  })

  // Each tab has its own nav history stack:

  .state('activate.request', {
    url: '/request',
    views: {
      'activate-request': {
        templateUrl: 'templates/activate_request.html',
        controller: 'ActivateRequestCtrl'
      }
    }
  })

  .state('activate.enter', {
      url: '/enter',
      views: {
        'activate-enter': {
          templateUrl: 'templates/activate_enter.html',
          controller: 'ActivateEnterCtrl'
        }
      }
    })

  .state('activate.help', {
    url: '/help',
    views: {
      'activate-help': {
        templateUrl: 'templates/activate_help.html',
        controller: 'ActivateHelpCtrl'
      }
    }
  })
*/
  .state('slider', {
    url: '/slider',
    templateUrl: 'templates/slider.html',
	controller: 'SliderCtrl'
  })

  .state('pre_loading', {
    url: '/pre_loading',
    templateUrl: 'templates/pre_loading.html',
	controller: 'PreLoadingCtrl',
	cache:false
  })
	
  .state('app', {
    url: '/app',
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: 'templates/home.html',
		controller: 'HomeCtrl'
      }
    }
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html',
		controller: 'SearchCtrl'
      }
    }
  })

  .state('app.district_detail', {
    url: '/district_detail/:selectedItem',
    views: {
      'menuContent': {
        templateUrl: 'templates/district_detail.html',
		controller: 'DistrictDetailCtrl'
      }
    }
  })

  .state('app.news', {
    url: '/news',
    views: {
      'menuContent': {
        templateUrl: 'templates/news.html',
		controller: 'NewsCtrl'
      }
    }
  })

  .state('app.news_detail', {
    url: '/news_detail/:selectedItem',
    views: {
      'menuContent': {
        templateUrl: 'templates/news_detail.html',
		controller: 'NewsDetailCtrl'
      }
    }
  })

  .state('app.agenda', {
    url: '/agenda',
    views: {
      'menuContent': {
        templateUrl: 'templates/agenda.html',
		controller: 'AgendaCtrl'
      }
    }
  })

  .state('app.agenda_detail', {
    url: '/agenda_detail/:selectedItem',
    views: {
      'menuContent': {
        templateUrl: 'templates/agenda_detail.html',
		controller: 'AgendaDetailCtrl'
      }
    }
  })

  .state('app.addagenda', {
    url: '/addagenda',
    views: {
      'menuContent': {
        templateUrl: 'templates/addagenda.html',
		controller: 'AddagendaCtrl'
      }
    }
  })
  
  .state('app.Circler_detail', {
    url: '/Circler_detail/:selectedItem',
    views: {
      'menuContent': {
        templateUrl: 'templates/circler_detail.html',
		controller: 'CirclerDetailCtrl'
      }
    }
  })

  .state('app.Circler_detail_edit', {
    url: '/Circler_detail_edit/:selectedItem',
    views: {
      'menuContent': {
        templateUrl: 'templates/circler_detail_edit.html',
		controller: 'CirclerDetailEditCtrl'
      }
    }
  })

  .state('app.Circlers_on_map', {
    url: '/Circlers_on_map/:selectedItem',
    views: {
      'menuContent': {
        templateUrl: 'templates/circlers_on_map.html',
		controller: 'CirclersOnMapCtrl'
      }
    }
  })

  .state('app.Circle_detail', {
    url: '/Circle_detail/:selectedItem',
    views: {
      'menuContent': {
        templateUrl: 'templates/circle_detail.html',
		controller: 'CircleDetailCtrl'
      }
    }
  })


  .state('app.ntr', {
    url: '/ntr',
    views: {
      'menuContent': {
        templateUrl: 'templates/ntr.html',
		controller: 'NTRCtrl'
      }
    }
  })

  .state('app.rti', {
    url: '/rti',
    views: {
      'menuContent': {
        templateUrl: 'templates/rti.html',
		controller: 'RTICtrl'
      }
    }
  })

  .state('app.articles', {
    url: '/articles/:selectedItem',
    views: {
      'menuContent': {
        templateUrl: 'templates/articles.html',
		controller: 'ArticleCtrl'
      }
    }
  })

    .state('app.Circle', {
    url: '/Circle',
    views: {
      'menuContent': {
        templateUrl: 'templates/circle.html',
    controller: 'CircleCtrl'
      }
    }
  })

    .state('app.secret', {
    url: '/secret',
    views: {
      'menuContent': {
        templateUrl: 'templates/secret.html',
    controller: 'SecretCtrl'
      }
    }
  })

    .state('app.ladies', {
    url: '/tafelaars/:selectedItem',
    views: {
      'menuContent': {
        templateUrl: 'templates/ladies.html',
    controller: 'CircledetailMembersCtrl'
      }
    }
  })

  .state('app.statistics', {
    url: '/statistics',
    views: {
      'menuContent': {
        templateUrl: 'templates/statistics.html',
		controller: 'StatisticCtrl'
      }
    }
  })
  
  ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

});


//Manually imported the angular-chart.js library because no CDN hosted it.
!function(t){"use strict";"function"==typeof define&&define.amd?define(["angular","chart.js"],t):"object"==typeof exports?module.exports=t(require("angular"),require("chart.js")):t(angular,Chart)}(function(t,e){"use strict";function n(){var n={},r={Chart:e,getOptions:function(e){var r=e&&n[e]||{};return t.extend({},n,r)}};this.setOptions=function(e,r){return r?(n[e]=t.extend(n[e]||{},r),void 0):(r=e,n=t.extend(n,r),void 0)},this.$get=function(){return r}}function r(n){function r(t,e){return t&&e&&t.length&&e.length?Array.isArray(t[0])?t.length===e.length&&t[0].length===e[0].length:e.reduce(a,0)>0?t.length===e.length:!1:!1}function a(t,e){return t+e}function o(e,r,a){if(r.data&&r.data.length){r.getColour="function"==typeof r.getColour?r.getColour:l,r.colours=c(e,r);var o=a[0],u=o.getContext("2d"),s=Array.isArray(r.data[0])?g(r.labels,r.data,r.series||[],r.colours):p(r.labels,r.data,r.colours),f=t.extend({},n.getOptions(e),r.options),h=new n.Chart(u)[e](s,f);return r.$emit("create",h),["hover","click"].forEach(function(t){r[t]&&(o["click"===t?"onclick":"onmousemove"]=i(r,h,t))}),r.legend&&"false"!==r.legend&&v(a,h),h}}function i(t,e,n){return function(r){var a=e.getPointsAtEvent||e.getBarsAtEvent||e.getSegmentsAtEvent;if(a){var o=a.call(e,r);t[n](o,r),t.$apply()}}}function c(r,a){for(var o=t.copy(a.colours||n.getOptions(r).colours||e.defaults.global.colours);o.length<a.data.length;)o.push(a.getColour());return o.map(u)}function u(t){return"object"==typeof t&&null!==t?t:"string"==typeof t&&"#"===t[0]?s(d(t.substr(1))):l()}function l(){var t=[f(0,255),f(0,255),f(0,255)];return s(t)}function s(t){return{fillColor:h(t,.2),strokeColor:h(t,1),pointColor:h(t,1),pointStrokeColor:"#fff",pointHighlightFill:"#fff",pointHighlightStroke:h(t,.8)}}function f(t,e){return Math.floor(Math.random()*(e-t+1))+t}function h(t,e){return"rgba("+t.concat(e).join(",")+")"}function d(t){var e=parseInt(t,16),n=e>>16&255,r=e>>8&255,a=255&e;return[n,r,a]}function g(e,n,r,a){return{labels:e,datasets:n.map(function(e,n){var o=t.copy(a[n]);return o.label=r[n],o.data=e,o})}}function p(t,e,n){return t.map(function(t,r){return{label:t,value:e[r],color:n[r].strokeColor,highlight:n[r].pointHighlightStroke}})}function v(t,e){var n=t.parent(),r=n.find("chart-legend"),a="<chart-legend>"+e.generateLegend()+"</chart-legend>";r.length?r.replaceWith(a):n.append(a)}function y(t,e,n){Array.isArray(n.data[0])?t.datasets.forEach(function(t,n){(t.points||t.bars).forEach(function(t,r){t.value=e[n][r]})}):t.segments.forEach(function(t,n){t.value=e[n]}),t.update(),n.$emit("update",t)}function C(t){return!t||Array.isArray(t)&&!t.length||"object"==typeof t&&!Object.keys(t).length}return function(e){return{restrict:"CA",scope:{data:"=",labels:"=",options:"=",series:"=",colours:"=?",getColour:"=?",chartType:"=",legend:"@",click:"=",hover:"="},link:function(n,a){function i(r,i){if(!C(r)&&!t.equals(r,i)){var u=e||n.chartType;u&&(c&&c.destroy(),c=o(u,n,a))}}var c,u=document.createElement("div");u.className="chart-container",a.replaceWith(u),u.appendChild(a[0]),"object"==typeof window.G_vmlCanvasManager&&null!==window.G_vmlCanvasManager&&"function"==typeof window.G_vmlCanvasManager.initElement&&window.G_vmlCanvasManager.initElement(a[0]),n.$watch("data",function(t,i){if(t&&t.length&&(!Array.isArray(t[0])||t[0].length)){var u=e||n.chartType;if(u){if(c){if(r(t,i))return y(c,t,n);c.destroy()}c=o(u,n,a)}}},!0),n.$watch("series",i,!0),n.$watch("labels",i,!0),n.$watch("options",i,!0),n.$watch("colours",i,!0),n.$watch("chartType",function(e,r){C(e)||t.equals(e,r)||(c&&c.destroy(),c=o(e,n,a))}),n.$on("$destroy",function(){c&&c.destroy()})}}}}e.defaults.global.responsive=!0,e.defaults.global.multiTooltipTemplate="<%if (datasetLabel){%><%=datasetLabel%>: <%}%><%= value %>",e.defaults.global.colours=["#97BBCD","#DCDCDC","#F7464A","#46BFBD","#FDB45C","#949FB1","#4D5360"],t.module("chart.js",[]).provider("ChartJs",n).factory("ChartJsFactory",["ChartJs",r]).directive("chartBase",["ChartJsFactory",function(t){return new t}]).directive("chartLine",["ChartJsFactory",function(t){return new t("Line")}]).directive("chartBar",["ChartJsFactory",function(t){return new t("Bar")}]).directive("chartRadar",["ChartJsFactory",function(t){return new t("Radar")}]).directive("chartDoughnut",["ChartJsFactory",function(t){return new t("Doughnut")}]).directive("chartPie",["ChartJsFactory",function(t){return new t("Pie")}]).directive("chartPolarArea",["ChartJsFactory",function(t){return new t("PolarArea")}])});