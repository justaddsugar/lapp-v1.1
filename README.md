**Welcome to LAPP repository**

We recommend you follow these steps to keep our repo neat and tidy:

>1. Check out a ticket from Jira, possibly comment on it, if the description of the ticket is too vague, to keep future references as memorable as possible.
>2. Create a branch with the ticket name (@3rand prefers the ticket url as branch name)
>3. Make your changes in this brach (lets refer to it as _work_ branch). Depending on the case usually ONE the following is true:
>  - the ticket is a small issue and the branch only gets 1 commit. In this case use again the ticket url as commit message.
>  - the ticket is a complicated issue with more than 1 commit. In this case enter descriptive messages on commits.
>  - there are lots of small tickets. Then create a branch with descriptive name (not url) and enter many commits each with 1 ticket url.
>4. Once finished with work branch, push your local changes to bitbucket branch with the same name as your work branch.
>5. Create a pull request, to merge _work_ with master. Optionally select to 'close the brach after merge', so we dont have too many old branches around.
>  - if code review procedures are to be followed assign this pull request to your Reviewer. They will merge the pull-request.
>  - if not merge the pull request yourself and @mention by tag any people who might need to be notified (only in particular scenarios eg if you changed a colleagues code radically or have made a chage that might be of large impact).
>6. You may then close the Jira ticket after merge, and reference in jira which pull-request resolves the ticket.